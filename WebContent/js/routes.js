﻿'use strict';

define([
		'app', //생성한 앵귤러 모듈에 루트를 등록하기 위해 임포트
		'route-config' //루트를 등록하는 routeConfig를 사용하기 위해 임포트,
	],

	function (app, routeConfig) {

		//app은 생성한 myApp 앵귤러 모듈
		return app.config(function ($routeProvider) {

			$routeProvider.when('/', routeConfig.config('../partials/main/main.html', '', {
				directives: [], 
				services: [], 
				filters: []
			}));

			$routeProvider.when('/signup', routeConfig.config('../partials/signup/signup_form.html', 'controllers/SignUpController', {
				directives: ['directives/compare-to'], 
				services: [], 
				filters: []
			}));

			$routeProvider.when('/language/new', routeConfig.config('../partials/language/new/new.html', 'controllers/LanguageNewController', {
				directives: [], 
				services: ['services/profileService', 'services/optionValuesCollection', 'services/lylService'], 
				filters: []
			}));

			$routeProvider.when('/language/new/:newLngId', routeConfig.config('../partials/language/new/steps.html', 'controllers/StepsController', {
				directives: ['directives/wizard-form', 'directives/price-formatter', 'directives/cp-event-form', 'directives/photo-upload'], 
				services: [], 
				filters: []
			}, {canAccess: function ($rootScope, $q, $http, $route, Constants){
					var deffered = $q.defer();
		           	$http({
						    url: Constants.Restful.LYL_RES_AUTH_URL, 
						    method: "GET",
						    params: {newLngId: $route.current.params.newLngId}
						 })
		                .success(function(data) {
		                	$rootScope.$broadcast('updateCSS', ['css/fullcalendar.css']);
		                    deffered.resolve({answer: 'yes'});	
		                }).error(function (data) {
		                	deffered.resolve({answer: 'no'});	
		                });
			        
			        return deffered.promise;
				}
			}
			));

			$routeProvider.when('/m/mymenu',routeConfig.config('../partials/m/mymenu.html','controllers/MymenuController',{
				directives: ['directives/bind-compiled-html', 'directives/skype-username-validity'],
				services: ['services/notiMessageService', 'services/profileService', 'services/optionValuesCollection', 'services/scrollTo'],
				filters: []
			}, null,false));

			//기본 경로 설정
			$routeProvider.otherwise({redirectTo:'/'});
		}).
		run(function ($rootScope, $location, AuthService, ngDialog) {

			$rootScope.$on('user.isLoggedIn', function (event, isLogged) {
				AuthService.isAuthenticated = isLogged;

				if(!isLogged){

	        		if($location.path().indexOf('/signup') != -1) { // 로그인 안한 상태에서 현재 url이 '/signup'을 포함한 path라면
	        			return;
	        		}
	        		$rootScope.$evalAsync(function () { 	// 라우팅 막기 event.preventDefault() in $locationChangeStart
			            redirectToHome();
			        }); 
	        	} else {
	        		if($location.path().indexOf('/signup') != -1) { // 로그인 한 상태에서 현재 url이 '/signup'을 포함한 path라면
	        			redirectToHome();
					}
	        	}
			});

			 $rootScope.$on('$locationChangeSuccess', function(event, next, current) {
			 	$rootScope.actualLocation = $location.path();
			 	AuthService.authenticate();
		    });     

		   $rootScope.$watch(function () {return $location.path()}, function (newLocation, oldLocation) {
		        if($rootScope.actualLocation === newLocation) {	// 뒤로가기 체크
		            ngDialog.closeAll();	// 다이얼로그가 열려져 있다면 다 닫음
		        }
		    });

		   $rootScope.$watch(function () { return $location.search(); }, function (newValue, oldValue) {
		   		myMemuParamCheck(newValue);	// 허용된 url 이외의 것으로 접근할 경우 홈으로 리다이렉트
		   });

		   function myMemuParamCheck (param) {
				if(param.t === undefined)
					return;

				if(param.t !== 'notifications' && param.t !== 'inbox' && 
					param.t !== 'class' && param.t !== 'profile' && param.t !== 'account'){
					redirectToHome();
				} else {
					if(param.htc !== 'true' && param.htc !== 'false') {
						redirectToHome();
					}
				}
			}

			function redirectToHome() {
				$location.$$search = {};
				$location.path('/');
			}

		});
});