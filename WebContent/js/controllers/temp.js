				// 가격control에 미리 값을 설정해 봄(로드할 경우)
            	//$document.find('#price').val(Number(250).toFixed(2));
            	form =  $('#wizard');
            	form.validate({
		        	errorClass: 'wizard-invalid',
		        	errorPlacement: function(error, element) {
						// Append error within linked label
						$( element ).closest( "form" ).find( "label[for='" + element.attr( "id" ) + "']" ).parent()
									.append( error );
					},
		    		errorElement: "div",
			        rules: {
						schedule: "scheduleStepValidator"
					}
		    	});

			    var id = 0;
			    var boolArr=[];
			    var count=0;
			    var result=false;
			  
				$('#scheduleView').fullCalendar({
					header: {
						left: '',
						center: '',
						right: ''
					},
					defaultView: 'agendaWeek',
			     	viewRender: function(view, element) {
				       	element.find('.fc-day-header.fc-widget-header.fc-sun').html('Sun');
				       	element.find('.fc-day-header.fc-widget-header.fc-mon').html('Mon');
				       	element.find('.fc-day-header.fc-widget-header.fc-tue').html('Tue');
				       	element.find('.fc-day-header.fc-widget-header.fc-wed').html('Wed');
				       	element.find('.fc-day-header.fc-widget-header.fc-thu').html('Thu');
				       	element.find('.fc-day-header.fc-widget-header.fc-fri').html('Fri');
				       	element.find('.fc-day-header.fc-widget-header.fc-sat').html('Sat');

				       	element.css({
				       		backgroundColor: '#fff',
				       		border: '1px solid #ccc',
				       		borderTop: '2px solid #E74C3C'
				   		});
				       
			     	},
			     	eventAfterRender: function ( event, element, view ) { 
			     		element.parent().parent().parent().parent().parent().css('padding','0px');
			     		if(element.hasClass('fc-day-grid-event')) {
			     			element.css({
				     			backgroundColor: '#60A5FF',
				     			height: '20px',
				     			top: '12px',
				     			border: '0px'
				     		});
			     		}

			     		$('input[name="schedule"]').valid();
			     	},
			     	timezone: false,
			     	eventStartEditable: false,
					editable: true,
			     	fixedWeekCount: false,
			     	defaultDate: '2015-2-1',
			     	selectable: true,
			     	selectHelper: true,
			     	eventLimit: true,
			     	lang: 'en',
			     	height: 550,
			     	eventRender: function (event, element, view) {
			     		if(event.source == undefined || event.source.events.length == 0) {
			     			$scope.scheduleStepValid = false;
			     		} else {
			     			$scope.scheduleStepValid = true;
			     		}

		                element.css({backgroundColor: '#60A5FF', border: '0px'});
		                element.find('.fc-bg').css('opacity', '0');
		                element.find('.fc-time').css({textAlign: 'center', display: 'block'});
		                
				    },
			     	select: function(start, end){

			       		$('#scheduleView').fullCalendar('clientEvents', function(event) {
				            var prevStart = event.start;
				            var prevEnd = event.end;
				            if((start >= prevStart && start <= prevEnd && end >= prevStart && end <= prevEnd) || (end > prevStart && start <= prevStart) || (start < prevEnd && end >= prevEnd)){
				                boolArr.push(true);
				                
				                console.log("this area event is already exist can't use.");
				            }else{
				                boolArr.push(false);
				            }
				         
				            count++;
				             
				            if(count == event.source.events.length){
				                if(!checkIfEventAlreadyExist()){
				                  	result = false;
				                }else{
				                  	result = true;
				                }
				                
				                boolArr = [];
				                count =0;
				            }
			       		});
			       
				        var eventData;
				        /*var m = $.fullCalendar.moment(start.toDate()).utcOffset(9);*/

				        if(!result){
				          	eventData = {
				             	id: id++,
				             	title: '',
				             	start: start,
				             	end: end,
				             	className: 'eventBox'
				          	};
				          	$('#scheduleView').fullCalendar('renderEvent', eventData, true);  
				       	}

			        	$('#scheduleView').fullCalendar('unselect');
			    	},
				    /*eventMouseover: function(calEvent, domEvent) {
				      var layer =	"<div id='events-layer' class='fc-transparent' style='position:absolute; width:100px; height:100%; top:-1px; text-align:right; z-index:100'> <a> <img border='0' style='padding-right:5px;' src='http://png-4.findicons.com/files/icons/1008/quiet/48/no.png' onClick='deleteEvent("+calEvent.id+");'></a></div>";
				      $(this).append(layer);
				    },   
				    eventMouseout: function(calEvent, domEvent) {
				      $("#events-layer").remove();
				    },*/
			    	eventClick: function(calEvent, jsEvent, view) {
			    		if(calEvent.source.events.length == 1){
		        			$scope.scheduleStepValid = false;
		          			result = false;

		          			$('input[name="schedule"]').valid();
		        		}
			       		//$(this).animate({ opacity: 0 }, 1000, function() {
			        		
		        		$('#scheduleView').fullCalendar('removeEvents', calEvent.id);
			      		//});
			    	}
			    /*events: [
								{
									title: 'All Day Event',
									start: '2015-2-01'
								}]*/

				});
			  
			   	function checkIfEventAlreadyExist(){
			       console.log(boolArr); 
			       for(var i in boolArr){
			         
			         if(boolArr[i]===true){
			           return true;
			         }
			       }
			     
			       return false;
			   	}