'use strict';

define(['controllers/regular/SubmenuController', 'controllers/regular/EditProfileController', 'controllers/regular/NotiController'], function () {
	
	//컨트롤러 선언
	function _controller($scope, $rootScope, $routeParams, $location, $timeout, $q, NotiMessageService, ProfileService) {
		$scope.skypeValidUserNameArray = [];
		$scope.currentTab = $location.search().t;
		$scope.noti = {};	// 알림 메시지객체
		$scope.profilePromise;	// profile promist obj

		var promise, prevTab,
		underlineEl, tabEl,	// 이전 요소 저장하기 위한 변수
		notiTabUnderline, inboxTabUnderline, classTabUnderline, profileTabUnderline, accountTabUnderline;

		init();

		function init () {
			notiTabUnderline = $('.mymenu-noti-underline'), inboxTabUnderline = $('.mymenu-inbox-underline'),
			classTabUnderline = $('.mymenu-class-underline'), profileTabUnderline = $('.mymenu-profile-underline'),
			accountTabUnderline = $('.mymenu-account-underline');
		}

		function isTabAnimating (element) {
			if(element.is(':animated'))
				return true;
			else
				return false;
		}

		function startTabAnimating (element, properties) {
			element.css({width: '14px', left: '63px'});
			element.animate(properties, {duration: 150, queue: false, easing: 'easeOutBack'});
		}

		function stopTabAnimating (element) {
			element.animate({width: '14px', left: '63px'}, {duration: 150, queue: false, complete: function () {
				element.css({width: '0px'});
			}});
		}

		$('#notifications_tab').hover(function () {
			if(prevTab !== undefined && isTabAnimating(prevTab)) {	// 이전 탭 애니메이션 중이면
				stopTabAnimating(prevTab);
			} else {
				$scope.setTab();	// 선택되어있던 탭 제거 (잔재된 탭 효과)	
			}

			
			promise = $timeout(function () { // 0.3초 이하의 호버링은 애니메이팅 하지 않음
				startTabAnimating(notiTabUnderline, {width: '89px', left: '26px'});
			}, 300);
		}, function () {
			prevTab = notiTabUnderline;
			$timeout.cancel(promise);

			promise.then(function() {
				if($scope.currentTab !== 'notifications'){	// 클릭된 탭은 언더라인이 유지되어야함
					stopTabAnimating(notiTabUnderline);
				}
			});
		});

		$('#inbox_tab').hover(function () {
			if(prevTab !== undefined && isTabAnimating(prevTab)) {
				stopTabAnimating(prevTab);
			} else {
				$scope.setTab();	
			}

			promise = $timeout(function () {
				startTabAnimating(inboxTabUnderline, {width: '41px', left: '50px'});
			}, 300);
		}, function () {
			prevTab = inboxTabUnderline;
			$timeout.cancel(promise);

			promise.then(function() {
				if($scope.currentTab !== 'inbox'){
					stopTabAnimating(inboxTabUnderline);
				}
			});
		});

		$('#class_tab').hover(function () {
			if(prevTab !== undefined && isTabAnimating(prevTab)) {
				stopTabAnimating(prevTab);
			} else {
				$scope.setTab();	
			}
			
			promise = $timeout(function () {
				startTabAnimating(classTabUnderline, {width: '35px', left: '53px'});
			}, 300);
		}, function () {
			prevTab = classTabUnderline;
			$timeout.cancel(promise);

			promise.then(function() {
				if($scope.currentTab !== 'class'){
					stopTabAnimating(classTabUnderline);
				}
			});
		});

		$('#profile_tab').hover(function () {
			if(prevTab !== undefined && isTabAnimating(prevTab)) {
				stopTabAnimating(prevTab);
			} else {
				$scope.setTab();	
			}
			
			promise = $timeout(function () {
				startTabAnimating(profileTabUnderline, {width: '48px', left: '46px'});
			}, 300);
		}, function () {
			prevTab = profileTabUnderline;
			$timeout.cancel(promise);

			promise.then(function() {
				if($scope.currentTab !== 'profile'){
					stopTabAnimating(profileTabUnderline);
				}
			});
		});

		$('#account_tab').hover(function () {
			if(prevTab !== undefined && isTabAnimating(prevTab)) {
				stopTabAnimating(prevTab);
			} else {
				$scope.setTab();	
			}

			promise = $timeout(function () {
				startTabAnimating(accountTabUnderline, {width: '59px', left: '41px'});
			}, 300);
		}, function () {
			prevTab = accountTabUnderline;
			$timeout.cancel(promise);

			promise.then(function() {
				if($scope.currentTab !== 'account'){
					stopTabAnimating(accountTabUnderline);
				}
			});
		});

		$scope.setTab = function (tab) {
			if(tab === 'notifications') {
				underlineEl =  $('.mymenu-noti-underline');
				underlineEl.css({width: '89px', left: '26px'});
				tabEl = $('#notifications_tab');
				tabEl.css({color: '#000'});
			} else if (tab === 'inbox') {
				underlineEl = $('.mymenu-inbox-underline');
				underlineEl.css({width: '41px', left: '50px'});
				tabEl = $('#inbox_tab');
				tabEl.css({color: '#000'});
			} else if (tab === 'class') {
				underlineEl = $('.mymenu-class-underline');
				underlineEl.css({width: '35px', left: '53px'});
				tabEl = $('#class_tab');
				tabEl.css({color: '#000'});
			} else if (tab === 'profile') {
				underlineEl = $('.mymenu-profile-underline');
				underlineEl.css({width: '48px', left: '46px'});
				tabEl = $('#profile_tab');
				tabEl.css({color: '#000'});
			} else if (tab === 'account') {
				underlineEl = $('.mymenu-account-underline');
				underlineEl.css({width: '59px', left: '41px'});
				tabEl = $('#account_tab');
				tabEl.css({color: '#000'});
			}else{
				underlineEl.css({width: '0px', left: '70px'});
				tabEl.css({color: '#6C5E5E'});
			}
		}

		function findEl (name) {
			if(name === 'notifications') {
				return {
					underLine: $('.mymenu-noti-underline'),
					tab: $('#notifications_tab')
				};
			} else if (name === 'inbox') {
				return {
					underLine: $('.mymenu-inbox-underline'),
					tab: $('#inbox_tab')
				};
			} else if(name === 'class') {
				return {
					underLine: $('.mymenu-class-underline'),
					tab: $('#class_tab')
				};
			} else if(name === 'profile') {
				return {
					underLine: $('.mymenu-profile-underline'),
					tab: $('#profile_tab')
				};
			} else if(name === 'account') {
				return {
					underLine: $('.mymenu-account-underline'),
					tab: $('#account_tab')
				};
			}
		}

		function paramCheck (param) {
			if(param.t === undefined)
				return;

			if(param.t === 'notifications') {
				// 알림메시지와 inbox에 새로온 메시지를 가져와야함
				
				var promise = NotiMessageService.getPendingMessage();
				promise.then(function (data) {
					if(data.code == 200) {
						$scope.noti.alert_msgs = data.responseData.noti_msgs;

						//noti.new_inbox_msg = data.responseData.new_inbox_msg;
					} else {
						console.log(data.msg);
					}
				});

			} else if (param.t === 'inbox') {
				// 메시지를 가져와야함
				// restful api 추가해야함
			} else if(param.t === 'class') {	// class화면 일 때, cst 파라미터가 적절한 값인지 확인
				if(param.cst !== 'yl' && param.cst !== 'yg' && param.cst !== 'yt' && param.cst !== 'rm') {
					$location.url($location.path('/'));
				}
			} else if(param.t === 'profile') {
				$scope.disabled = true;

				if(param.pst === 'ep') {
					var promise = ProfileService.getUserProfile();
					promise.then(function (data) {
						var deffered = $q.defer();
						
						if(data.code == 200) {
							deffered.resolve(data.responseData.profile);
							$scope.disabled = false;
						} else {
							deffered.reject(data.msg);
							console.log(data.msg);
						}

						$scope.profilePromise = deffered.promise;
					});
				}

				if(param.pst !== 'ep' && param.pst !== 'pts' && param.pst !== 'rvs') {
					$location.url($location.path('/'));
				}
			} else if (param.t === 'account') {
				if(param.ast !== 'pm' && param.ast !== 'th' && param.ast !== 'sts') {
					$location.url($location.path('/'));
				}
			}
		}

		$scope.$watch(function () { return $location.search(); }, function (newValue, oldValue) {

			paramCheck(newValue);	// 파라미터 체크

			$scope.currentTab = newValue.t;

			if(newValue.t != oldValue.t){	// t파라미터에 변화가 있다면
				var el, ishorizontalTabClicked = Boolean(newValue.htc === 'true');

				if(ishorizontalTabClicked){
					el = findEl(newValue.t);
					underlineEl = el.underLine;
					tabEl = el.tab;	
					
				}else {
					el = findEl(oldValue.t);
					underlineEl = el.underLine;
					tabEl = el.tab;	
					$scope.setTab();	// 잔재된 탭 효과 없앰
				}
			}
		}, true);

		$scope.$watch('currentTab', function (newValue) {
			if(underlineEl == undefined){
				$scope.setTab(newValue);				
			}
			
			if($location.search().htc == 'true'){
				return;
			}

			$scope.setTab(newValue);
		});

		$scope.changeMenuState = function (state) {
			if (state === 'class'){
				$location.search({t: state, cst: $scope.subState.cst, htc: 'true'});	// htc= horizontal tab click
			} else if (state === 'profile'){
				$location.search({t: state, pst: $scope.subState.pst, htc: 'true'});	
			} else if (state === 'account'){
				$location.search({t: state, ast: $scope.subState.ast, htc: 'true'});	
			} else {
				$location.search({t: state, htc: 'true'});	
			}
			
		}

		$scope.showMenuContent = function () {
			return "partials/m/" + $location.search().t + "/" + $location.search().t + ".html";
		}

		$scope.showSubmenuContent = function () {
			var submenuView;
			var url = $location.url();
			// 서브메뉴를 포함한 화면에서 noti나 inbox화면으로 이동할 때 발생되는 문제해결
			if(url.split('&').length < 2) {	
				return;
			}

			var paramName = url.split('&')[1].split('=')[0];	// url에서 두 번째 파라미터 추출
			
			if(paramName === 'cst') {
				var subState = $location.search().cst;

				if (subState === 'yl') {
					submenuView = 'sm_your_listings.html';
				} else if (subState === 'yt') {
					submenuView = 'sm_your_takers.html';
				} else if (subState === 'yg') {
					submenuView = 'sm_your_givers.html';
				} else if (subState === 'rm') {
					submenuView = 'sm_request_messages.html';
				}
			} else if (paramName === 'pst'){
				var subState = $location.search().pst;

				if (subState === 'ep') {
					submenuView = 'sm_edit_profile.html';
				} else if (subState === 'pts') {
					submenuView = 'sm_photos.html';
				} else if (subState === 'rvs') {
					submenuView = 'sm_reviews.html';
				}
			} else if (paramName === 'ast') {
				var subState = $location.search().ast;

				if (subState === 'pm') {
					submenuView = 'sm_payout_method.html';
				} else if (subState === 'th') {
					submenuView = 'sm_transaction_history.html';
				} else if (subState === 'sts') {
					submenuView = 'sm_settings.html';
				}
			}

			if(!submenuView)
				return;
			else
				return "partials/m/" + $location.search().t + "/" + submenuView;
		}

	}
	//생성한 컨트롤러 리턴
	return _controller;
});