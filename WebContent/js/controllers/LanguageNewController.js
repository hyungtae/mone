'use strict';

define(['ladda'], function (Ladda) {
	
	//컨트롤러 선언
	function _controller($scope, $rootScope, $timeout, $q, $location, ProfileService, LYLService, OptionValuesCollection, ngDialog) {
		
		var profilePromise, NewModalController = ['$scope', function($scope){

			$scope.confirm = function () {
				$scope.closeThisDialog();
			}

		}];

		$scope.msg = {};
		$scope.mainstep = {};
		$scope.disabled = true;

		init();

		function init() {
			var config = {
				template: 'partials/language/new/guideDialog.html',
				className: 'ngdialog-theme-plain',
				scope: $scope,
				controller: NewModalController,
				plain: false,
				showClose: false,
				closeByEscape: false,
				closeByDocument: false,
				overlay: true
			}

			if($scope.user.emailConfirm == 0) {
				$scope.msg = "Before you start, you need to activate your email account. Please confirm your email address.";
				$scope.need = "email check";
				ngDialog.open(config);
			} else { 
				var promise = ProfileService.getUserProfile();
				promise.then(function (data) {
					var deffered = $q.defer();
					if(data.code == 200) {
						if(data.responseData.profile.profileSetting == 0) {	// 프로필이 미설정 되어 있다면
							$scope.msg = "Before you start, you need to complete your profile."
							$scope.need = "profile";
							ngDialog.open(config);
						} else {
							deffered.resolve(data.responseData.profile);
						}

					} else {
						console.log(data.msg);
						$scope.msg = "Error occured..."
						$scope.need = "error";
						ngDialog.open(config);
					}

					profilePromise = deffered.promise;
					
				});
			}

		}

	    // lazy load select box
	    $timeout(function () {
	    	$scope.willTeachLngItems = OptionValuesCollection.getLanguageItems();
	    	$scope.targetCountryItems = OptionValuesCollection.getTargetCountryItems();
	    	$scope.limitItems = OptionValuesCollection.getLimitItems();
	    	$scope.timezoneItems = OptionValuesCollection.getTimezoneItems();
	    	$scope.$watch(function () {return profilePromise}, function (promise) {
	    		if(promise !== undefined){
		    		promise.then(function (profile) {
		    			$scope.disabled = false;
			    		$scope.firstLng = profile.firstLng;
			    		setSelectedItem($scope.timezoneItems, profile.timezone, 'timezone');
			    	});	
		    	}
	    	});
		});

		$scope.$watch('mainstep.willTeachLng', function (willTeachLng) {
			if($scope.firstLng == undefined)
				return;
			
			$('#nativeLabel').fadeOut();

			if(willTeachLng === $scope.firstLng) {
				$scope.mainstep.isNative = 1;
			}else{
				$scope.mainstep.isNative = 0;
			}

			$('#nativeLabel').fadeIn();
		});

		$scope.addMainStepToLYL = function () {
			var promise = LYLService.addMainStepToLYL($scope.mainstep);
			var l = Ladda.create(document.querySelector('#mNewContinueBtn'));
			l.start();

			promise.then(function (data) {
				if(data.code == 200) {
					var newLngId = data.responseData.newLngId;
					$location.replace();
					$location.path('/language/new/' + newLngId);
				} else {
					console.log(data.msg);
				}

				l.stop();
			})
		}

		function setSelectedItem (selectItems, selectedItem, property) {
			for(var i in selectItems) {
				if(selectItems[i].value === selectedItem){
					$scope.mainstep[property] = selectItems[i];
					break;
				}
			}
		}

	}

	//생성한 컨트롤러 리턴
	return _controller;
});