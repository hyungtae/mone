'use strict';

define(['ladda'], function (Ladda) {
	
	//컨트롤러 선언
	function _controller($scope, $resource, AuthService, ngDialog, $rootScope, $location) {
		$scope.focus = true;
		$scope.emailErrorText = '';
		$scope.data = {};				// 서버에서의 응답을 받을 객체
		$scope.isAlertOpened = false;	// 경고창이 열려있는지 아닌지
		$scope.signupDiaogId;

		var credentials = {email: '', pwd: ''};

		$scope.$watch('userForm.s_email.$error.required', function(required){
			if(required){
				$scope.emailErrorText = 'email address is empty.';
			}else{
				$scope.emailErrorText = 'invalid email address.';
			}
		});
		
		$rootScope.$on('ngDialog.opened', function (e, $dialog) {
			
			if($dialog.attr('id') === $scope.signupDiaogId){
				$scope.isAlertOpened = true;	// 경고창이 열려있으면 엔터키를 쳐도 sumit되지 않음
			}
		});
		
		$rootScope.$on('ngDialog.closed', function (e, $dialog) {
			
			if($dialog.attr('id') === $scope.signupDiaogId){
				$scope.isAlertOpened = false;	// 경고창이 닫히면 원래대로 엔터키를 쳤을 때 submit이 동작

				if($scope.data.code == 200){	// 성공적으로 가입한 상태에서 닫으려고 할 때
					$rootScope.$broadcast('user.isLoggedIn', true);
					$scope.user.emailConfirm = 0;	// 이메일인증 기본값 0-비인증, 1-인증
					$scope.nav.currentTpl = 'partials/navbar/main_nav_after_login.html';
					$rootScope.$apply(function() {
						$location.replace();
			        	$location.path('/');
			      	});					
				}
			}
		});
		
		var SignupModalController = ['$scope', '$rootScope', function($scope, $rootScope){

			$scope.confirm = function () {
				var code = $scope.data.code;

				if(code == 400) {			// 악의적 스크립트를 포함하고 있을 때 (Bad request)
					$scope.signup.email = '';
					$scope.signup.pwd = '';
					$scope.signup.pwd2 = '';
				}else if(code == 6001) {	// 이미 이메일이 존재할 때
					$scope.signup.email = '';
				}

				$scope.closeThisDialog();
				$('#email').focus();

			}

			
		}];

		$scope.moneSignup = function(){
			credentials.email = $scope.signup.email;
			credentials.pwd = $scope.signup.pwd;

			var promise = AuthService.signup(credentials);
			var l = Ladda.create(document.querySelector( '#mSignupBtn' ));
			l.start();

			promise.then(function (data) {	// @param data - response obj from server

				var config = {
					template: 'partials/signup/alertDialog.html',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					controller: SignupModalController,
					plain: false,
					showClose: false,
					closeByEscape: false,
					closeByDocument: false,
					overlay: false
				};

				$scope.data = data;

				if(data.code == 200){	// 가입 성공
					$scope.user.email = credentials.email;
					$scope.user.displayName = $scope.split(credentials.email);
					var promise = AuthService.requestEmailConfirmURL(credentials.email);
					promise.then(function (data) {
						if(data.code == 200) {
							console.log(data.msg);
						} else {
							console.log(data.msg);
						}
					});

					config.template = 'partials/signup/signupOkDialog.html'
					config.showClose = true;
					config.closeByEscape = true;
					config.closeByDocument = true;
				}

				l.stop();
				
				var dialog = ngDialog.open(config);
				$scope.signupDiaogId = dialog.id;
			});
		}

		
	}

	//생성한 컨트롤러 리턴
	return _controller;
});