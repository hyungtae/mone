'use strict';

define(['ladda', 'fullcalendar'], function (Ladda) {
	
	//컨트롤러 선언
	function _controller($scope, $rootScope, $document, $location, $timeout, canAccess, $compile) {
		$scope.listingLng = {price:50};
		$scope.wizardForm = $('#wizard');
		$scope.scheduleStepValid = false;
		$scope.curriculums = [];
		$scope.cid = 0;
		$scope.events;
		var currentDay = 1;

  		if(canAccess.answer == 'no') {
			$location.path('/');	
  		}

  		$.validator.setDefaults({ 
		    ignore: ":disabled,:hidden:not(.not-ignore)"
		});

  		$.validator.addMethod(
	        "scheduleStepValidator", 
	        function(value, element) {
	        	if($('fieldset#wizard-p-2').attr('aria-hidden') === 'true')
	        		return true;
	            return $scope.scheduleStepValid;
	        }, 'Please, set your availability.'
	    );

	    function initValidate () {
	    	$scope.wizardForm.validate({
	        	errorClass: 'wizard-invalid',
	        	errorPlacement: function(error, element) {
					// Append error within linked label
					$( element ).closest( "form" ).find( "label[for='" + element.attr( "id" ) + "']" ).parent()
								.append( error );
				},
	    		errorElement: "div",
		        rules: {
					schedule: "scheduleStepValidator"
				}
	    	});
	    }

	    function setScheduleValid(flag) {
	    	$scope.scheduleStepValid = flag;
	    }

	    function removePushedTempEvents () {
	    	if($scope.events === undefined)
	    		return;
	    	
	    	for(var i = $scope.events.length - 1; i > 0; i--) {
    			if($scope.events[i].pushed)
    				$scope.events.splice(i, 1);
    		}
	    }

	    function setScheduleCalendar () {
			var eventId = 1, judge=[], count=0, isEventOveray=false;

			function checkIfEventAlreadyExist(){
		       for(var i in judge){
		         if(judge[i]===true)
		           return true;
		       }
		     
		       return false;
		   	}

			$('#scheduleView').fullCalendar({
				header: {
					left: '',
					center: '',
					right: ''
				},
				defaultView: 'agendaWeek',
		     	viewRender: function(view, element) {
		     		$('#scheduleView').fullCalendar('gotoDate', '2015-02-01');
		     		//$('#scheduleView').fullCalendar('gotoDate', 2015, 2, 1);

			       	element.find('.fc-day-header.fc-widget-header.fc-sun').html('Sun');
			       	element.find('.fc-day-header.fc-widget-header.fc-mon').html('Mon');
			       	element.find('.fc-day-header.fc-widget-header.fc-tue').html('Tue');
			       	element.find('.fc-day-header.fc-widget-header.fc-wed').html('Wed');
			       	element.find('.fc-day-header.fc-widget-header.fc-thu').html('Thu');
			       	element.find('.fc-day-header.fc-widget-header.fc-fri').html('Fri');
			       	element.find('.fc-day-header.fc-widget-header.fc-sat').html('Sat');

			       	element.css({
			       		backgroundColor: '#fff',
			       		border: '1px solid #ccc',
			       		borderTop: '2px solid #E74C3C'
			   		});
			       
		     	},
		     	eventAfterRender: function ( event, element, view ) {
		     		$scope.events = event.source.events;

		     		element.parent().parent().parent().parent().parent().css('padding','0px');
		     		if(element.hasClass('fc-day-grid-event')) {
		     			element.css({
			     			backgroundColor: '#60A5FF',
			     			height: '20px',
			     			top: '12px',
			     			border: '0px'
			     		});
		     		}

		     		$('input[name="schedule"]').valid();
		     	},
		     	timezone: false,
		     	eventStartEditable: false,
				editable: true,
		     	fixedWeekCount: false,
		     	//defaultDate: $.fullCalendar.moment('2015-2-1'),
		     	selectable: true,
		     	selectHelper: true,
		     	eventLimit: true,
		     	lang: 'en',
		     	height: 550,
		     	eventRender: function (event, element, view) {
		     		//$scope.scheduleStepValid = true;
		     		setScheduleValid(true);

	                element.css({backgroundColor: '#60A5FF', border: '0px'});
	                element.find('.fc-bg').css('opacity', '0');
	                element.find('.fc-time').css({textAlign: 'center', display: 'block'});
	                
			    },
		     	select: function(start, end){

		       		$('#scheduleView').fullCalendar('clientEvents', function(event) {
			            var prevStart = event.start, prevEnd = event.end;

			            if((start >= prevStart && start <= prevEnd && end >= prevStart && end <= prevEnd) || (end > prevStart && start <= prevStart) || (start < prevEnd && end >= prevEnd)){
			                judge.push(true);
			            }else{
			                judge.push(false);
			            }
			         
			            count++;
			             
			            if(count == event.source.events.length){
			                isEventOveray = checkIfEventAlreadyExist();
			                judge = [];
			                count =0;
			            }
		       		});
		       
			        var eventData;

			        if(!isEventOveray){
			          	eventData = {
			             	id: eventId++,
			             	title: '',
			             	start: start,
			             	end: end,
			             	className: 'eventBox'
			          	};
			          	$('#scheduleView').fullCalendar('renderEvent', eventData, true);  
			       	}

		        	$('#scheduleView').fullCalendar('unselect');
		    	},
		    	eventClick: function(calEvent, jsEvent, view) {
		    		if(calEvent.source.events.length == 1){
	        			setScheduleValid(false);
	        			// $scope.scheduleStepValid = false;

	          			isEventOveray = false;

	          			$('input[name="schedule"]').valid();
	        		}
		        		
	        		$('#scheduleView').fullCalendar('removeEvents', calEvent.id);
		    	}
			});
	    }

	    function setCurriculumCalendar () {
	    	var eventdayList = [];
	    	for(var i in $scope.events) {
	    		var s_after7days = $scope.events[i].start.clone().add(7,'days');
	    		var s_after14days = $scope.events[i].start.clone().add(14,'days');
	    		var s_after21days = $scope.events[i].start.clone().add(21,'days');
	    		var e_after7days = $scope.events[i].end.clone().add(7,'days');
	    		var e_after14days = $scope.events[i].end.clone().add(14,'days');
	    		var e_after21days = $scope.events[i].end.clone().add(21,'days');

	    		$scope.events.push({start:s_after7days, end:e_after7days, allDay: $scope.events[i].allDay, pushed: true});
	    		$scope.events.push({start:s_after14days, end:e_after14days, allDay: $scope.events[i].allDay, pushed: true});
	    		$scope.events.push({start:s_after21days, end:e_after21days, allDay: $scope.events[i].allDay, pushed: true});
	    		
	    	}

	    	for(var i in $scope.events) {
	    		if($scope.events[i].allDay) {
	    			var t = $scope.events[i].end - $scope.events[i].start;
	    			var d = Math.floor(t / (24 * 60 * 60 * 1000)) - 1;  // 시작일과 종료일간의 차(일수)
	    			for(var j = 0; j <= d; j++) {
	    				var initial_day = $scope.events[i].start.clone();
	    				eventdayList.push(initial_day.add(j, 'days').format('YYYY-MM-DD'));
	    			}

	    		}else{
	    			var start = $scope.events[i].start.clone().format('YYYY-MM-DD');
		    		var end = $scope.events[i].end.clone().format('YYYY-MM-DD');

		    		var isStartDuplicated = false, isEndDuplicated = false;

		    		for(var i in eventdayList) {
		    			if(eventdayList[i] == start)
		    				isStartDuplicated = true;
		    			
		    			if (eventdayList[i] == end)
		    				isEndDuplicated = true;
		    		}

		    		if(!isStartDuplicated && !isEndDuplicated) {
		    			if (start == end) {
							eventdayList.push(start);
						} else {
							eventdayList.push(start);
							eventdayList.push(end);
						}	
		    		} else if (!isEndDuplicated) {	// 뒤에 것만 추가
		    			eventdayList.push(end);
		    		}	
	    		}
	    	}

	    	var events = [];
	    	for(var i in eventdayList) {
	    		$scope.curriculums.push({title:'', description:''});
	    		
	    		events.push({
	             	id: ++i,
	             	title: '',
	             	start: eventdayList[--i],
	             	end: eventdayList[i],
	             	curriculums: $scope.curriculums
	          	});
	    	}

	    	

	    	$('#curriculumView').fullCalendar('destroy');

			$('#curriculumView').fullCalendar({
				header: {
					left: '',
					center: '',
					right: ''
				},
				defaultView: 'month',
		     	viewRender: function(view, element) {
		     		$('#curriculumView').fullCalendar('gotoDate', '2015-02-01');
		     		//$('#curriculumView').fullCalendar('gotoDate', 2015, 2, 1);

			       	element.find('.fc-day-header.fc-widget-header.fc-sun').html('Sun');
			       	element.find('.fc-day-header.fc-widget-header.fc-mon').html('Mon');
			       	element.find('.fc-day-header.fc-widget-header.fc-tue').html('Tue');
			       	element.find('.fc-day-header.fc-widget-header.fc-wed').html('Wed');
			       	element.find('.fc-day-header.fc-widget-header.fc-thu').html('Thu');
			       	element.find('.fc-day-header.fc-widget-header.fc-fri').html('Fri');
			       	element.find('.fc-day-header.fc-widget-header.fc-sat').html('Sat');

			       	element.css({
			       		backgroundColor: '#fff',
			       		border: '1px solid #ccc',
			       		borderTop: '2px solid #E74C3C'
			   		});
			       
		     	},
		     	timezone: false,
		     	eventStartEditable: false,
				editable: false,
		     	fixedWeekCount: false,
		     	//defaultDate: $.fullCalendar.moment('2015-2-1'),
		     	selectable: false,
		     	selectHelper: false,
		     	eventLimit: true,
		     	lang: 'en',
		     	height: 550,
		     	events: events,
		     	dayClick: function (date, jsEvent, view) {
		     		console.log('dayClick event is called in curriculumView');
		     	},
		     	eventAfterRender: function ( event, element, view ) { 

		     	},
		     	eventRender: function (event, element, view) {
		     		var boxCss = {
	                	border: '0px',
  						backgroundColor: '#bce8f1',
  						height: '123px',
  						top: '-25px',
  						opacity: '.3',
  						margin: '0',
  						borderRadius: '0',
  					  	filter: 'alpha(opacity=30)'};

		     		if (event.id == currentDay) {
		     			boxCss.border =  '2px solid #00F';
		     			boxCss.height = '118px';
		     			boxCss.top = '-24px';
		     		}

		     		element.css(boxCss);
		     		
			    },
		    	eventClick: function(calEvent, jsEvent, view) {
		    		currentDay = calEvent.id;
		    		$scope.$apply(function(){$scope.cid = calEvent.id - 1;});
		    		/*$scope.curriculums[cid].title = calEvent.curriculums[cid].title;
		    		$scope.curriculums[cid].desciption = calEvent.curriculums[cid].description;*/
		    		
		    		$('#curriculumView').fullCalendar('rerenderEvents');
		    	}
			});

	    }
	    

		$scope.stepOptions = {
            headerTag: 'h2',
            bodyTag: 'fieldset',
            transitionEffect: 'slideLeft',
            enableFinishButton: true,
            enablePagination: true,
            enableAllSteps: false,
            autoFocus: true,
            titleTemplate: '#title#',
            cssClass: 'tabcontrol',
            startIndex: 4,
            labels: {
                finish: 'List Language'
            },
            onInit: function (event, currentIndex) {
            	initValidate();
            	if (currentIndex == 2){
            		setScheduleCalendar();
            	} else if (currentIndex == 3) {
            		setCurriculumCalendar();
            	}
        		
        		$('#curri_container').append($compile('<cp-event-form></cp-event-form>')($scope));
        		$('#photo_upload_container').append($compile('<photo-upload></photo-upload>')($scope));
        		
            },
            onStepChanging: function (event, currentIndex, newIndex) {
				// Allways allow previous action even if the current form is not valid!
		        if (currentIndex > newIndex) {
		            return true;
		        }

		        return $scope.wizardForm.valid();

            },
            onStepChanged: function (event, currentIndex, priorIndex) {
            	if (currentIndex == 2){
            		removePushedTempEvents();
            		setScheduleCalendar();
            	} else if (currentIndex == 3) {
            		setCurriculumCalendar();

            		/*if ($('#curri_container').has('cp-event-form').length == 0) {
            			$('#curri_container').append($compile('<cp-event-form></cp-event-form>')($scope));
            		}*/
            	}
            }
        };
	}

	//생성한 컨트롤러 리턴
	return _controller;
});