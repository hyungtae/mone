'use strict';

define(['ladda'], function (Ladda) {

	angular.module('moneApp').controller('EditProfileController', 
		['$scope', '$timeout', '$location', '$q', 'ProfileService', 'OptionValuesCollection', 'ScrollTo', function($scope, $timeout, $location, $q, ProfileService, OptionValuesCollection, ScrollTo){
	    
	    $scope.profile = {};
	    $scope.validation = {};
	    $scope.isInvalid = false;
	    console.log($scope.user.emailConfirm);
	    
	    $timeout(function () { // lazy load select's option values
	    	$scope.genderItems = OptionValuesCollection.getGenderItems();
		    $scope.monthItems = OptionValuesCollection.getMonthItems();
		    $scope.dayItems = OptionValuesCollection.getDayItems();
		    $scope.yearItems = OptionValuesCollection.getYearItems();
		    $scope.nationalityItems = OptionValuesCollection.getNationalityItems();
		    $scope.firstLngItems = OptionValuesCollection.getLanguageItems();
		    $scope.yaiLngsItems = OptionValuesCollection.getYaiLngsItems();
		    $scope.secondLngsItems = OptionValuesCollection.getSecondLnaguagesItems();
		    $scope.timezoneItems = OptionValuesCollection.getTimezoneItems();
    		$scope.$watch('profilePromise', function (promise) {
		    	if(promise !== undefined){
		    		promise.then(function (profile) {
			    		console.log(profile.timezone);
			    		var date = new Date(profile.birth_dt);
			    		$scope.profile.firstName = profile.firstName;
			    		$scope.profile.lastName = profile.lastName;
			    		$scope.profile.email = profile.email;
			    		$scope.profile.city = profile.city;
			    		$scope.profile.skype_username = profile.skype_username;
			    		$scope.profile.yaiLngs = profile.yaiLngs;
			    		$scope.profile.secondLngs = profile.secondLngs;
			    		setSelectedItem($scope.genderItems, profile.gender, 'gender');
						setSelectedItem($scope.monthItems, date.getMonth()+1, 'month');
						setSelectedItem($scope.dayItems, date.getDate(), 'day');
						setSelectedItem($scope.yearItems, date.getFullYear(), 'year');
			    		setSelectedItem($scope.nationalityItems, profile.nationality, 'nationality');
			    		setSelectedItem($scope.firstLngItems, profile.firstLng, 'firstLng');
			    		setSelectedItem($scope.timezoneItems, profile.timezone, 'timezone');
			    	});	
		    	}
		    });

		});

		function setSelectedItem (selectItems, selectedItem, property) {
			for(var i in selectItems) {
				if(selectItems[i].value === selectedItem){
					$scope.profile[property] = selectItems[i];
					break;
				}
			}
		}

		$scope.verifySkypeUserName = function (SKYPE_USER_NAME) {
			
			var promise = ProfileService.verifySkypeUserName(SKYPE_USER_NAME);
			var l = Ladda.create(document.querySelector('#verifyBtn'));
			l.start();

			promise.then(function (data) {
				if(data.code == 200) {
					if(data.responseData.isVerified_SUN) {
						$scope.isInvalid = false;	// alert hide
						$scope.skypeValidUserNameArray.push($scope.profile.skype_username);	// 유효한 스카이프 유저이름을 배열에 추가
					} else {
						$scope.isInvalid = true;	// alert show$
						ScrollTo.animate($('#helper'));
					}
				} else {
					console.log(data.msg);
				}

				l.stop();
			});
		}

		function checkForm () {
			if($scope.pfForm.$invalid) {
				if($scope.pfForm.firstName.$invalid)
					$scope.validation.cantSubmit_firstName = true;

				if ($scope.pfForm.lastName.$invalid)
					$scope.validation.cantSubmit_lastName = true;

				if ($scope.pfForm.gender.$invalid)
					$scope.validation.cantSubmit_gender = true;

				if ($scope.pfForm.month.$invalid)
					$scope.validation.cantSubmit_month = true;

				if ($scope.pfForm.day.$invalid)
					$scope.validation.cantSubmit_day = true;

				if ($scope.pfForm.year.$invalid)
					$scope.validation.cantSubmit_year = true;

				if ($scope.pfForm.email.$invalid)
					$scope.validation.cantSubmit_email = true;

				if ($scope.pfForm.nationality.$invalid)
					$scope.validation.cantSubmit_nationality = true;

				if ($scope.pfForm.city.$invalid)
					$scope.validation.cantSubmit_city = true;

				if ($scope.pfForm.firstLng.$invalid)
					$scope.validation.cantSubmit_firstLng = true;

				if ($scope.pfForm.skype_username.$invalid) {
					$scope.validation.cantSubmit_skype_username = true;
					$scope.pfForm.skype_username.$setPristine();
				}

				ScrollTo.animate($('#helper'));

				return false;
			}
			return true;
		}

		$scope.saveProfile = function () {
			// 폼검사
			if(!checkForm())
				return;

			var promise = ProfileService.editUserProfile($scope.profile);
			var l = Ladda.create(document.querySelector( '#profileSaveBtn' ));
			l.start();

			promise.then(function (data) {
				if(data.code == 200) {
					console.log(data.msg);
				} else {
					console.log(data.msg);
				}

				l.stop();
			});
		}

	}]);
});