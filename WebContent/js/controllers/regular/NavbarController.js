'use strict';

define(['app', 'ladda'], function (app, Ladda) {
	app.controller('NavbarController', ['$scope', '$rootScope', 'ngDialog', '$facebook', 'AuthService', '$route', '$location',
		 function($scope, $rootScope, ngDialog, $facebook, AuthService, $route, $location){
			
			$scope.loginDialogId;
			
			var LoginModalController = ['$scope', 'ngDialog', 'AuthService', '$location', '$timeout', function($scope, ngDialog, AuthService, $location, $timeout) {
				$scope.focus = true;
				var credentials = {email: '', pwd: '', remember: false};

				// private methods
				var _ = {
					showAlert: function(element, warningMsg, completeCallback) {
						$scope.warningMsg = warningMsg;
						element.fadeIn('slow', completeCallback);
					},
					hideAlert: function(element, completeCallback) {
						$timeout(function(){
							element.fadeOut('slow', completeCallback);
							
						}, 3000);
					},
					checkFormValidation: function () {
						var me = this;
						var target = $('.login-modal-alertbox');

						if($scope.loginForm.$invalid){
							me.showAlert(target, 'Invalid form.', function(){
								me.hideAlert(target, function(){$scope.warningMsg = '';})
							});
						
						return false;
						}

						return true;
					}
					
				};

				$scope.$watch('loginForm.email.$invalid', function (newValue) {
					if(newValue) {
						$scope.loginForm.email.$setPristine();
					}
				});

				$scope.$watch('loginForm.pwd.$invalid', function (newValue) {
					if(newValue) {
						$scope.loginForm.pwd.$setPristine();
					}
				})

				$scope.moneLogin = function(){
					// 폼 유효성 검사에 실패한다면 로그인을 진행하지 않음
					if(!_.checkFormValidation())
						return;
					// 폼에서 받은 정보
					credentials.email = $scope.login.email;
					credentials.pwd = $scope.login.pwd;
					credentials.remember = $scope.login.remember;
					// 로그인
					var promise = AuthService.login(credentials);
					var l = Ladda.create(document.querySelector( '#mSigninBtn' ));
					l.start();

					promise.then(function (data) {
						$scope.data = data;
						var target = $('.login-modal-alertbox');

						if(target.is(':animated')){
							target.stop();

						}

						if(data.code == 200){
							$scope.$emit('user.moneLoggedIn');
							$rootScope.$broadcast('user.isLoggedIn', true);
							$scope.user.email = credentials.email;
							$scope.user.displayName = $scope.split(credentials.email);
							$scope.nav.currentTpl = 'partials/navbar/main_nav_after_login.html';
							$location.path('/');
							
							$scope.closeThisDialog();
						}else if(data.code == 400){
							_.showAlert(target, data.msg, function(){
								_.hideAlert(target, function(){$scope.warningMsg = '';})
							});
						}else if(data.code == 6000){
							_.showAlert(target, data.msg, function(){
								_.hideAlert(target, function(){$scope.warningMsg = '';})
							});

						}else if(data.code == 6002){
							_.showAlert(target, data.msg, function(){
								_.hideAlert(target, function(){$scope.warningMsg = '';})
							});

						}else if( data.code == 500){
							_.showAlert(target, data.msg, function(){
								_.hideAlert(target, function(){$scope.warningMsg = '';})
							});
						}

						l.stop();

					});
				}
			}];

			$scope.dropdownShow = function () {
				$('#navdropdown.dropdown-menu').css('display', 'block');
			}

			$scope.dropdownHide = function () {
				$('#navdropdown.dropdown-menu').css('display', 'none');
				
			}

			$scope.moneLogout = function () {
				$scope.dropdownHide();

				var promise = AuthService.logout();
				promise.then(function (data) {
					$scope.data = data;

					if(data.code == 200){
						// document.cookie = "m_suid=; path=/Mone/; expires=Thu, 01 Jan 1970 00:00:00 UTC";
						console.log($scope.user);
						$rootScope.$broadcast('user.isLoggedIn', false);
						$scope.user.email = null, $scope.user.pwd = null,$scope.user.displayName = '', $scope.user.emailConfirm = null;
						$scope.nav.currentTpl = 'partials/navbar/main_nav.html';
						$scope.subState.cst= 'yl', $scope.subState.pst = 'ep', $scope.subState.ast = 'pm';
					}else{
						console.log('server response code'+data.code+"!");
					}
				});
			}

			// 로그인창 오픈(Navbar에서 Signin버튼 클릭시 실행)
			$scope.openLoginModal = function () {
				$scope.$emit('updateCSS', ['css/ngDialog.css', 'css/ngDialog-theme-plain.css']);
				//$scope.value = true;
				console.log($scope.login);
				var dialog = ngDialog.open({
					template: 'partials/main/loginDialog.html',
					className: 'ngdialog-theme-plain',
					scope: $scope,
					controller: LoginModalController,
					plain: false,
					showClose: false,
					closeByDocument: true,
					closeByEscape: true,
					appendTo: false
				});

				$scope.loginDialogId = dialog.id;
			};

			$scope.listLanguage = function () {
				if(!AuthService.isAuthenticated){
					$scope.openLoginModal();
				}

			}

			// 다이얼로그 콜백( 열렸을 때 페이스북 sdk 동적으로 로딩)
			$rootScope.$on('ngDialog.opened', function (e, $dialog) {
				if($dialog.attr('id') === $scope.loginDialogId){
					loadFacebookSDK();
				}
			    
			});

			function loadFacebookSDK(){
				// Load the facebook SDK asynchronously
			    (function(){
			        // If we've already installed the SDK, we're done
			        if (document.getElementById('facebook-jssdk')) {return;}

			        // Get the first script element, which we'll use to find the parent node
			        var firstScriptElement = document.getElementsByTagName('script')[0];

			        // Create a new script element and set its id
			        var facebookJS = document.createElement('script'); 
			        facebookJS.id = 'facebook-jssdk';

			        // Set the new script's source to the source of the Facebook JS SDK
			        facebookJS.src = '//connect.facebook.net/en_US/all.js';

			        // Insert the Facebook JS SDK into the DOM
			        firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
			    }());
			}


			function apiClientLoaded() {
		        gapi.client.plus.people.get({userId: 'me'}).execute(handleResponse);
		    }

		    function handleResponse(resp) {
		        console.log(resp.displayName+' is logged in with google account...');
		    }

			$scope.$on('event:google-plus-signin-success', function (event, authResult) {
				// User successfully authorized the G+ App!
				console.log('Signed in!');
				gapi.client.load('plus', 'v1', apiClientLoaded);
			});

			$scope.$on('event:google-plus-signin-failure', function (event, authResult) {
				// User has not authorized the G+ App!
				console.log('Not signed into Google Plus.');
			});

			$scope.$on('fb.auth.login', function(event, response, FB){
				console.log(response + 'is logged in with facebook...');
				$facebook.api('/me').then(
					function(response){
						console.log(response.name+' is logged in');
					},
					function(err){
						console.log('erro is occured when fb login do');
					}
				);
			});

			$scope.fbLogin = function() {
				console.log('log in with facebook call!');
				$facebook.login().then(function() {
					//AuthService.isAuthenticated = true;
					console.log("logged in with facebook!");		
				  	//refresh();
				});
			}

			$scope.logout = function() {

				$facebook.logout().then(function(response){
					//AuthService.isAuthenticated = false;
					console.log('logged out from facebook');
				});
				
			}

		}
	]);
});