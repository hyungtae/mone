'use strict';

define([], function () {

	angular.module('moneApp').controller('SubmenuController', ['$scope', '$location', function($scope, $location){
		$scope.currentSubmenuTab;	// st = subtab

		$scope.twinkle = function ($event,bool) {
			var target = $event.target;
			var subtab = target.id;
			
			if($scope.currentSubmenuTab != subtab){
				if(bool) {
					$(target).css({color: '#D2CCCD'});
				} else {
					$(target).css({color: '#82888a'});
				}	
			}
			
		}

		$scope.$watch('currentSubmenuTab', function (newValue) {
			var mState = $location.search().t;	// 메뉴 상태

			if(mState === 'profile') {
				$scope.setPfSubMenuTab(newValue);	
			} else if (mState === 'class') {
				$scope.setClSubMenuTab(newValue);	
			} else if (mState === 'account') {
				$scope.setAcSubMenuTab(newValue);
			}
			
		})

		$scope.$watch(function () { return $location.search(); }, function (newValue, oldValue) {
			
			if(newValue.pst != undefined){
				$scope.currentSubmenuTab = newValue.pst;
				$scope.subState.pst = newValue.pst;	
			}	
			
			if(newValue.cst != undefined){
				$scope.currentSubmenuTab = newValue.cst;
				$scope.subState.cst = newValue.cst;	
			}

			if(newValue.ast != undefined){
				$scope.currentSubmenuTab = newValue.ast;
				$scope.subState.ast = newValue.ast;	
			}			
			
		});

		$scope.setClSubMenuTab = function (tab) {	// Class 
			if(tab === 'yl') {	// SubTab - Your Listings
				$('#yl').css({color: '#000', 'font-weight': 'bold'});
				$('#yt').css({color: '#82888a', 'font-weight': 'normal'});
				$('#yg').css({color: '#82888a', 'font-weight': 'normal'});
				$('#rm').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'yt') {	// SubTab - your Takers
				$('#yl').css({color: '#82888a', 'font-weight': 'normal'});
				$('#yt').css({color: '#000', 'font-weight': 'bold'});
				$('#yg').css({color: '#82888a', 'font-weight': 'normal'});
				$('#rm').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'yg') { // SubTab - Your Givers
				$('#yl').css({color: '#82888a', 'font-weight': 'normal'});
				$('#yt').css({color: '#82888a', 'font-weight': 'normal'});
				$('#yg').css({color: '#000', 'font-weight': 'bold'});
				$('#rm').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'rm') { // SubTab - Request Messages
				$('#yl').css({color: '#82888a', 'font-weight': 'normal'});
				$('#yt').css({color: '#82888a', 'font-weight': 'normal'});
				$('#yg').css({color: '#82888a', 'font-weight': 'normal'});
				$('#rm').css({color: '#000', 'font-weight': 'bold'});
			}
		}

		$scope.setPfSubMenuTab = function (tab) {	// Profile
			if(tab === 'ep') {	// SubTab - Edit Profile
				$('#ep').css({color: '#000', 'font-weight': 'bold'});
				$('#pts').css({color: '#82888a', 'font-weight': 'normal'});
				$('#rvs').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'pts') {	// SubTab - Photos
				$('#ep').css({color: '#82888a', 'font-weight': 'normal'});
				$('#pts').css({color: '#000', 'font-weight': 'bold'});
				$('#rvs').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'rvs') { // SubTab - Reviews
				$('#ep').css({color: '#82888a', 'font-weight': 'normal'});
				$('#pts').css({color: '#82888a', 'font-weight': 'normal'});
				$('#rvs').css({color: '#000', 'font-weight': 'bold'});
			}
		}

		$scope.setAcSubMenuTab = function (tab) {	// Account
			if(tab === 'pm') {
				$('#pm').css({color: '#000', 'font-weight': 'bold'});
				$('#th').css({color: '#82888a', 'font-weight': 'normal'});
				$('#sts').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'th') {
				$('#pm').css({color: '#82888a', 'font-weight': 'normal'});
				$('#th').css({color: '#000', 'font-weight': 'bold'});
				$('#sts').css({color: '#82888a', 'font-weight': 'normal'});
			} else if (tab === 'sts') {
				$('#pm').css({color: '#82888a', 'font-weight': 'normal'});
				$('#th').css({color: '#82888a', 'font-weight': 'normal'});
				$('#sts').css({color: '#000', 'font-weight': 'bold'});
			}
		}

	}]);
});