'use strict';

define(['ladda'], function (Ladda) {

	angular.module('moneApp').controller('NotiController', ['$scope', '$rootScope', 'AuthService', 'ngDialog', function($scope, $rootScope, AuthService, ngDialog){
		
		var l, NotiModalController = ['$scope', function($scope){}];

		$scope.msgLoaded = false;

		$rootScope.$on('ngDialog.opened', function (e, $dialog) {
			if($dialog.attr('id') === $scope.guideDiaogId){
				l = Ladda.create(document.querySelector( '#mNotiGuideBtn' ));
				l.start();
			}
		});


		$scope.requestNewConfirmEmailUrl = function () {
			console.log($scope.user.email);
			var config = {
				template: 'partials/m/notifications/guideDialog.html',
				className: 'ngdialog-theme-plain',
				scope: $scope,
				controller: NotiModalController,
				plain: false,
				showClose: true,
				closeByEscape: true,
				closeByDocument: true,
				overlay: false
			},
			promise = AuthService.requestNewConfirmEmailUrl($scope.user.email);

			promise.then(function (data) {
				$scope.data = data;

				if(data.code == 200) {
					console.log(data.msg);

				} else {
					console.log(data.msg);
				}

				l.stop();
				$scope.msgLoaded = true;
			});

			var dialog = ngDialog.open(config);
			$scope.guideDiaogId = dialog.id;

			
		}

	}]);
});