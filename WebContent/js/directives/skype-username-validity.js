'use strict';

define([], function () {

    return ['skypeUsernameValidity', function () {
		return {
			restrict: 'A',
			require: "ngModel",
	        link: function(scope, element, attrs, ctrl) {
	            var validateParser = function (value) {
	            	var valid = false;
	            	for(var i in scope.skypeValidUserNameArray) {
	            		if(scope.skypeValidUserNameArray[i] === value){
	            			valid = true;
	            		}
	            	}

	            	ctrl.$setValidity("skypeValidity", valid);
	            	return value;
	            }
	            // 유효한 유저이름이 추가 되었을 때도 유효하게 바꿔준다
	            scope.$watch('skypeValidUserNameArray', function (newValue) {
	            	ctrl.$setValidity("skypeValidity", true);
	            }, true);

	            ctrl.$parsers.push(validateParser);
	        }
		}
	}]
})