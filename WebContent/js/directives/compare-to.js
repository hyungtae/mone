'use strict';

define([], function () {

    return ['compareTo', function () {
		return {
			restrict: 'A',
			require: "ngModel",
	        link: function(scope, element, attrs, ctrl) {
	            var firstPassword = '#' + attrs.compareTo;
	            element.add(firstPassword).on('keyup', function () {
	                scope.$apply(function () {
	                    ctrl.$setValidity('compareTo', element.val() === $(firstPassword).val());
	                });
	            });
	        }
		}
	}]
})