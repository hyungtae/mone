'use strict';

define([], function () {

    return ['cpEventForm', function ($compile) {

		return {
			restrict: 'E',
	        link: function(scope, element, attrs) {
	        	var template = angular.element('<div style="height: 280px;background-color: #E5E0A8;overflow: hidden;padding: 15px;border: 1px solid #BEBEBE;border-radius: 2px"><div class="curri-subject" style="height: 90px;"><div class="form-group"><label for="curri_title" style="font-weight: bold;font-size: 17px">TITLE</label><input id="curri_title" name="curri_title" ng-model="curriculums[cid].title" class="form-control"></div></div><div class="curri-content" style="height: 100%;"><div class="form-group"><label for="curri_description" style="font-weight: bold;font-size: 17px">DESCRIPTION</label><textarea id="curri_description" name="curri_description" ng-model="curriculums[cid].description" rows="5" class="form-control"></textarea></div></div></div>');
	        	console.log(scope.curriculums);
				console.log(scope.cid);
	            element.append($compile(template)(scope));
	    	}
	    }
	}]
})