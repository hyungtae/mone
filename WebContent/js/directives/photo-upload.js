'use strict';

define(['jquery-fileupload'], function () {

    return ['photoUpload', function ($compile, Constants) {

		return {
			restrict: 'E',
	        link: function(scope, element, attrs) {
	        	var template = angular.element('<div style="height: 360px;background-color: #fff;padding: 70px 20px 0px 50px;"><div id="photo_upload" style="height: 80%;width: 315px;float: left;background-color: #FFF;border: 3px dotted #C5BCBC;position: relative; overflow: hidden"><div style="height: 15px;padding: 20px;"><span class="glyphicon glyphicon-plus" style="float: right;font-size: 20pt;color: #5e5e5e"></span></div><div style="width: 1%;margin: auto"><span class="glyphicon glyphicon-picture" style="font-size: 77pt;color: #5E5E5E"></span></div><h2 style="text-align: center;font-weight: normal">Add a Photo</h2><input id="real_photo_upload" type="file" name="files" style="opacity:0;-moz-opacity:0;filter:alpha(opacity: 0);width: 100%; height: 100%;position: absolute;left: 0;top: 0;margin: 0;cursor: pointer" ></div></div>'),
	        	upload_container = template.find('#photo_upload'),
	        	upload = template.find('#real_photo_upload');
	        	
	        	upload_container.on('mouseenter', function () {
	        		$(this).css({'border': '3px solid #191247', 'color': '#191247', 'cursor': 'pointer'});
	        		$(this).find('h2').css('fontWeight', 'bold');
	        	});
	        	upload_container.on('mouseleave', function () {
	        		$(this).css({'border': '3px dotted #C5BCBC', 'color': '#333333', 'cursor': 'default'});
	        		$(this).find('h2').css('fontWeight', 'normal');

	        	});
	        	upload_container.on('click', function () {
	        		
	        	});

	        	upload.fileupload({
			        url: Constants.Restful.LYL_PHOTO_RES_URL,
			        autoUpload: false,
			        //acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			        //maxFileSize: 999000
			    }).on('fileuploadadd', function (e, data) {
			        console.log('photo is added...');
			        var uploadErrors = [], acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
			        if(data.originalFiles[0]['type'] == '' || data.originalFiles[0]['type'].length && !acceptFileTypes.test(data.originalFiles[0]['type'])) {
			            uploadErrors.push('Not an accepted file type');
			        }
			        if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 999000) {
			            uploadErrors.push('Filesize is too big');
			        }
			        
			        if(uploadErrors.length > 0) {
			            alert(uploadErrors.join("\n"));
			        } else {
			            data.submit();
			        }
			        /*data.submit()
			            .success(function (result, textStatus, jqXHR) { })
			            .error(function (jqXHR, textStatus, errorThrown) { })
			            .complete(function (result, textStatus, jqXHR) {console.log('submit success callback!'); });*/
			    }).on('fileuploadprocessalways', function (e, data) {
			        var index = data.index,
            		file = data.files[index];

            		console.log(file);
			    }).on('fileuploadprogressall', function (e, data) {
			       
			    }).on('fileuploaddone', function (e, data) {
			        console.log('successfully uploadded...');
			    }).on('fileuploadfail', function (e, data) {
			        console.log('error occured');
			    });

	            element.append($compile(template)(scope));
	    	}
	    }
	}]
})