'use strict';

/*
 * angular-google-plus-directive v0.0.1
 * ♡ CopyHeart 2013 by Jerad Bitner http://jeradbitner.com
 * Copying is an act of love. Please copy.
 */

angular.module('directive.g+signin', []).
  directive('googlePlusSignin', ['$window', function ($window) {
    var ending = /\.apps\.googleusercontent\.com$/;

    return {
      restrict: 'A',
      transclude: true,
      template: '<div id="gSigninBtn" class="btn btn-danger"><span class="icon-g"></span><span class="buttonText">Sign in with Google</span></div>',
      replace: true,
      link: function (scope, element, attrs, ctrl, linker) {
    
        var defaults = {
          clientid: '1036305148230-phkjs7tkrvqlmfhjqrn8rjgdcjlvgh30.apps.googleusercontent.com',
          callback: 'signinCallback',
          cookiepolicy: 'http://mone-global.com',
          requestvisibleactions: 'http://schemas.google.com/AddActivity',
          scope: 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/userinfo.email',
          approvalprompt: 'force'
        };
        
        // Asynchronously load the G+ SDK.
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/client:plusone.js?onload=handleLoad';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);

        linker(function(el, tScope){
          $window.handleLoad = function () {
            if (el.length) {
              element.append(el);
            }

            gapi.signin.render(element[0], defaults);
          } 
        });
        
      }
    }
}]).
  run(['$window','$rootScope',function($window, $rootScope) {
    $window.signinCallback = function (authResult) {
      if (authResult && authResult.access_token){
        $rootScope.$broadcast('event:google-plus-signin-success', authResult);
      } else {
        $rootScope.$broadcast('event:google-plus-signin-failure', authResult);
      }
    }; 
}]);