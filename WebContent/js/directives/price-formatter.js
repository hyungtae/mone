'use strict';

define([], function () {

    return ['priceFormatter', function ($document, $parse) {
		return {
			require: 'ngModel',
	        link: function(scope, element, attrs, ctrl) {

	        	$document.on('change', '#price', function(e) {
	        		var me = this;

	        		scope.$apply(function () {
	        			var min = parseFloat(attrs.min);
				    	var max = parseFloat(attrs.max);
				    	//var value = me.valueAsNumber;
				    	var value = Number(me.value);
				    	if(value < min)
				    		value = min;
				    	else if(value > max)
				    		value = max;
				    	$(me).val(value.toFixed(2));
	        		});
				});
	    	}
	    };
	}];
});