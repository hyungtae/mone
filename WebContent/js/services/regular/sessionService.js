'use strict';

define(['services/regular/authService', 'services/regular/constants'], function () {
	angular.module('moneApp.authService')
		.factory('SessionService', function(){

			return {
			    get: function(key) {
			      return sessionStorage.getItem(key);
			    },
			    set: function(key, val) {
			      return sessionStorage.setItem(key, val);
			    },
			    unset: function(key) {
			      return sessionStorage.removeItem(key);
			    }
			};
		});
})