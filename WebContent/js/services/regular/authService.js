'use strict';

define(['ngSanitize'], function () {

	angular.module('moneApp.authService', ['ngSanitize'])
		.factory('AuthService', function($rootScope, $resource, $sanitize, Constants){
			
			var usersRes = $resource(Constants.Restful.USER_RES_URL
				, null,
				{
			        login: { 
			        	url: Constants.Restful.LOGIN_RES_URL,
			        	method: 'POST'
			    	},
			    	logout: {
			    		url: Constants.Restful.LOGOUT_RES_URL,
			    		method: 'POST'
			    	},
			    	requestEmailConfirmURL: {
			    		url: Constants.Restful.REQUEST_CONFIRM_RES_URL,
			    		method: 'POST'
			    	},
			    	requestNewConfirmEmailUrl: {
			    		url: Constants.Restful.REQUEST_NEW_CONFIRM_RES_URL,
			    		method: 'POST'
			    	},
			    	authenticate: {
			    		url: Constants.Restful.AUTH_RES_URL,
			    		method: 'POST'/*,
			            // This is what I tried.
			            interceptor: {
			                response: function (data) {
			                    //console.log('response in interceptor', data);
			                    return data;
			                },
			                responseError: function (response) {
			                    if(response.status === 403) {
						        	console.log('403이니 네비게이션바를 로그인하기전 템플릿으로 써야함');
						        	//$location.path('/login');
						        }
						        return response;
			                }
			            }*/
			    	}
			    	
			    });

			var sanitizeCredentials = function(credentials) {
				try{
					var sanitizedEmail = $sanitize(credentials.email);
					var sanitizedPwd = $sanitize(credentials.pwd);
				} catch (err) {}
				finally {
					if(sanitizedPwd !== '')		// sanitize되지 못하는 것들은 그냥 값을 넘김
						sanitizedEmail = credentials.email, sanitizedPwd = credentials.pwd;
				}

				return {
					email: credentials.email,
					pwd: credentials.pwd,
					remember: credentials.remember,
					sanitizedData: {
						email: sanitizedEmail,
						pwd: sanitizedPwd
					}
			    };
			};

			function onSuccess(data, status, headers, config){
				var data = data;
				return data;
			}

			function onFailed(data, status, headers, config){
				var data = data.data;
				return data;
			}

			return {
				isAuthenticated: false,
				signup: function (credentials) {
				    return new usersRes(sanitizeCredentials(credentials)).$save()
						.then(onSuccess, onFailed);

				},
				requestEmailConfirmURL: function (email) {
					return usersRes.requestEmailConfirmURL(email).$promise.then(onSuccess, onFailed);
				},
				requestNewConfirmEmailUrl: function (email) {
					return usersRes.requestNewConfirmEmailUrl(email).$promise.then(onSuccess, onFailed);
				},
				login: function (credentials) {
					return usersRes.login(sanitizeCredentials(credentials)).$promise.then(onSuccess, onFailed);
			    },
			    logout: function () {
			        return usersRes.logout().$promise.then(onSuccess, onFailed);

			    },
			    authenticate: function() {
			        return usersRes.authenticate().$promise.then(
			        	function(data) {
			        		if(data.code == 200) {
				        		$rootScope.user.email = data.responseData.user.email;
								$rootScope.user.displayName = $rootScope.split(data.responseData.user.email);
								$rootScope.user.emailConfirm = data.responseData.user.email_confirm;
								$rootScope.nav.currentTpl = 'partials/navbar/main_nav_after_login.html';
								$rootScope.$broadcast('user.isLoggedIn', true);
							}
			        	}, function(data) {
			        		$rootScope.nav.currentTpl = 'partials/navbar/main_nav.html';
							$rootScope.$broadcast('user.isLoggedIn', false);
			        	});
			    }
			};
		});
});