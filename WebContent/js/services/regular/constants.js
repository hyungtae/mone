'use strict';

define(['services/regular/authService'], function () {
	angular.module('moneApp.authService')
		.factory('Constants', function(){
			var HOST = 'http://192.168.0.7:8080/Mone';
			return {
				Restful: {
					USER_RES_URL: HOST + '/api/users/:id',
			    	LOGIN_RES_URL: HOST + '/api/users/login',
			    	LOGOUT_RES_URL: HOST + '/api/users/logout',
			    	AUTH_RES_URL: HOST + '/api/users/auth',
			    	REQUEST_CONFIRM_RES_URL: HOST + '/api/users/request_email_confirm',
			    	REQUEST_NEW_CONFIRM_RES_URL: HOST + '/api/users/request_new_email_confirm',

			    	ALERT_MSG_RES_URL: HOST + '/api/users/noti',

			    	USER_PROFILE_RES_URL: HOST + '/api/profiles/',
			    	GET_USER_PROFILE_RES_URL: HOST + '/api/profiles/request_user_profile',
			    	EDIT_USER_PROFILE_RES_URL: HOST + '/api/profiles/edit',
			    	SKYPE_VALIDATOR_URL: HOST + '/api/skype/validator',

			    	LYL_RES_URL: HOST + '/api/lyl/',
			    	LYL_RES_AUTH_URL: HOST + '/api/lyl/auth',
			    	LYL_MAIN_RES_URL: HOST + '/api/lyl/main_step',
			    	LYL_DESCRIPTION_RES_URL: HOST + '/api/lyl/description_step',
			    	LYL_PRICING_RES_URL: HOST + '/api/lyl/pricing_step',
			    	LYL_SCHEDULE_RES_URL: HOST + '/api/lyl/schedule_step',
			    	LYL_COURSEPLAN_RES_URL: HOST + '/api/lyl/course_plan_step',
			    	LYL_PHOTO_RES_URL: HOST + '/api/lyl/photo_step',
			    	LYL_VOICEUPLOAD_RES_URL: HOST + '/api/lyl/voice_upload_step'
				}
			};
		});
});