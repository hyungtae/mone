'use strict';

define([], function () {

	return ['ScrollTo', function(){
		function scrollTo ($target) {
		    $('html, body').stop().animate({
		        'scrollTop': $target.offset().top
		    }, 100, 'swing', function () {
		        //window.location.hash = target;
		    });	
		}

		return {
			animate: function (target) {
				scrollTo(target);
		    }
		};

	}];

});