'use strict';

define([], function () {

	return ['ProfileService', function($resource, Constants){
		var profileRes = $resource(Constants.Restful.USER_PROFILE_RES_URL
				, null,
				{
			        verifySkypeUserName: { 
			        	url: Constants.Restful.SKYPE_VALIDATOR_URL,
			        	method: 'GET'
			    	},
			    	editUserProfile: {
			    		url: Constants.Restful.EDIT_USER_PROFILE_RES_URL,
			    		method: 'PUT'
			    	},
			    	getUserProfile: {
			    		url: Constants.Restful.GET_USER_PROFILE_RES_URL,
			    		method: 'GET'
			    	}
			    	
			    });
		function preProcess (profile) {
			var _profile = {}, birth_dt;
			if(typeof(profile['year']) == 'object' && typeof(profile['month']) == 'object' && typeof(profile['day']) == 'object') {
				birth_dt = profile.year.value + "-" + profile.month.value + "-" + profile.day.value;
			} else {
				birth_dt = profile.year + "-" + profile.month + "-" + profile.day;	
			}

			for(var property in profile) {
				if(property == 'year' || property == 'month' || property == 'day')
					continue;

				if(!Array.isArray(profile[property]) && typeof(profile[property]) == 'object') {
					_profile[property] = profile[property].value;
					continue;
				}

				_profile[property] = profile[property];
			}

			_profile.birth_dt = birth_dt;

			return _profile;
		}

		function onSuccess(data, status, headers, config){
			var data = data;
			return data;
		}

		function onFailed(data, status, headers, config){
			var data = data.data;
			return data;
		}

		return {
			verifySkypeUserName: function (SKYPE_USER_NAME) {
				return profileRes.verifySkypeUserName({new_username: SKYPE_USER_NAME}).$promise.then(onSuccess, onFailed);
		    },
		    editUserProfile: function (profile) {
		    	var _profile = preProcess(profile);
		    	return profileRes.editUserProfile(_profile).$promise.then(onSuccess, onFailed);
		    },
		    getUserProfile: function () {
		    	return profileRes.getUserProfile().$promise.then(onSuccess, onFailed);
		    }
		};

	}];

});