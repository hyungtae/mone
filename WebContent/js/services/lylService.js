'use strict';

define([], function () {

	return ['LYLService', function($resource, Constants){
		var lylRes = $resource(Constants.Restful.LYL_RES_URL
				, null,
				{
			        addMainStepToLYL: { 
			        	url: Constants.Restful.LYL_MAIN_RES_URL,
			        	method: 'POST'
			    	}

			    });

		function preProcess (step) {
			var _step = {};

			for(var property in step) {

				if(!Array.isArray(step[property]) && typeof(step[property]) == 'object') {
					_step[property] = step[property].value;
					continue;
				}

				_step[property] = step[property];
			}

			return _step;
		}

		function onSuccess(data, status, headers, config){
			var data = data;
			return data;
		}

		function onFailed(data, status, headers, config){
			var data = data.data;
			return data;
		}

		return {
			addMainStepToLYL: function (step) {
				var _step = preProcess(step);
				return lylRes.addMainStepToLYL(_step).$promise.then(onSuccess, onFailed);
		    }
		};

	}];

});