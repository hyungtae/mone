'use strict';

define([], function () {
	// value 서비스
	/*return ['tester', '아무거나'];*/

	// factory 서비스
	return ['tester', function(){
		var data = "아무거나";

		function getValue(){
			return data;
		}

		return {
			getValue: getValue
		};
	}];

	// provider 서비스
	/*return ['tester', function(){
		var data = "아무거나";

		function getValue(){
			return data;
		}

		this.$get = function(){
			return {
				getValue: getValue
			}
		}
	}];*/
})