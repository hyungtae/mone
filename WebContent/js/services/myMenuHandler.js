'use strict';

define([], function () {

	// factory 서비스
	return ['myMenuHandler', function(){
		var target = {};

		function _setTarget (underlineEl, u_css, tabEl, t_css) {
			target.underline = underlineEl;
			target.u_css = u_css	// underline css property
			target.tab = tabEl;
			target.t_css = t_css;	// tab css property
		}

		function _showSelectedTab () {
			target.underline.css(target.u_css);
			target.tab.css(target.t_css);
		} 

		function selectTab(tab){
			if(tab === 'notifications') {
				_setTarget($('.mymenu-noti-underline'), {width: '89px', left: '26px'},
					$('#notifications_tab'), {color: '#000'});
				_showSelectedTab();
				
			} else if (tab === 'inbox') {
				_setTarget($('.mymenu-inbox-underline'), {width: '41px', left: '50px'},
					$('#inbox_tab'), {color: '#000'});
				_showSelectedTab();
			} else if (tab === 'class') {
				_setTarget($('.mymenu-class-underline'), {width: '35px', left: '53px'},
					$('#class_tab'), {color: '#000'});
				_showSelectedTab();
			} else if (tab === 'profile') {
				_setTarget($('.mymenu-profile-underline'), {width: '48px', left: '46px'},
					$('#profile_tab'), {color: '#000'});
				_showSelectedTab();
			} else if (tab === 'account') {
				_setTarget($('.mymenu-account-underline'), {width: '59px', left: '41px'},
					$('#account_tab'), {color: '#000'});
				_showSelectedTab();
			}
		}

		function cancelTab (el) {
			el.underline.css({{width: '0px', left: '70px'}});
			el.tabEl.css({color: '#6C5E5E'});
		}

		return {
			animTarget: target,
			selectTab: selectTab,
			cancelTab: cancelTab
		};
	}];

})