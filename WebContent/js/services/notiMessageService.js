'use strict';

define([], function () {

	return ['NotiMessageService', function($resource, Constants){
		var alertMessageRes = $resource(Constants.Restful.ALERT_MSG_RES_URL
				, null,
				{
			        getPendingMessage: { 
			        	url: Constants.Restful.ALERT_MSG_RES_URL,
			        	method: 'POST'
			    	}
			    	
			    });

		function onSuccess(data, status, headers, config){
			var data = data;
			return data;
		}

		function onFailed(data, status, headers, config){
			var data = data.data;
			return data;
		}

		return {
			getPendingMessage: function () {
				return alertMessageRes.getPendingMessage().$promise.then(onSuccess, onFailed);
		    }
		};

	}];

});