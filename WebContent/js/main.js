'use strict';

requirejs.config({

	baseUrl:'js',

	paths:{

		'text': '../lib/require/text', 
		'jquery': '../lib/jquery/jquery-1.11.2.min',
		'jqueryUI': '../lib/jquery/jquery-ui-1.11.4',
		'jquery-fileupload': '../lib/jquery/jquery.fileupload',
		'jquery.ui.widget': '../lib/jquery/jquery.ui.widget',
		'jquery-iframe-transport': '../lib/jquery/jquery.iframe-transport',
		'jquery-cookie': '../lib/jquery/jquery.cookie-1.3.1',
		'jquery-steps': '../lib/jquery/jquery.steps',
		'jquery-validate': '../lib/jquery/jquery.validate',
		'fullcalendar': '../lib/jquery/fullcalendar',
		'moment': '../lib/moment/moment',
		'chosen': '../lib/jquery/chosen.jquery',
		'angular-chosen': '../lib/angular/angular-chosen',
		'ladda': '../lib/ladda/ladda.min',
		'spin': '../lib/ladda/spin.min',
		'ui.bootstrap': '../lib/bootstrap/bootstrap',
		'angular': '../lib/angular/v1.2.9_angular',
		'ngRoute' : '../lib/angular/v1.2.9_angular-route',
		'ngResource': '../lib/angular/v1.2.9_angular-resource',
		'ngSanitize': '../lib/angular/v1.2.9_angular-sanitize',
		'ngDialog' : '../lib/angular/ngDialog',
		'ngFacebook' : 'services/regular/ngFacebook',
		'ngGoogle' : 'directives/regular/google-plus-signin'
	},
	shim:{
		'ladda' :
			['spin']
		,
		'chosen': 
			['jquery']
		,
		'jquery-fileupload':
			['jquery', 'jquery.ui.widget', 'jquery-iframe-transport']
		,
		'fullcalendar': 
			['jquery', 'moment']
		,
		'jquery-steps': 
			['jquery', 'jquery-cookie']
		,
		'jquery-validate':
			['jquery']
		,	
		'angular-chosen':
			['chosen']
		,
		'jqueryUI': 
			['jquery']
		,
		'ui.bootstrap': 
			['jquery']
		,
		'angular':{
			deps:['jquery'],
			exports:'angular'
		},
		'ngRoute': 
			['angular']
		,
		'ngResource':
			['angular']
		,

		'ngSanitize':
			['angular']
		,
		'ngDialog':
			['angular']
		,
		'ngFacebook':
			['angular']
		,
		'ngGoogle':
			['angular']	
		,
		'app':{
			deps:['angular']
		},
		'routes':{
			deps:['angular']
		}
	}
});


requirejs( [
		'text', 
		'jquery',
		'angular', 
		'jqueryUI',
		'ui.bootstrap',
		'app', //app.js
		'controllers/regular/NavbarController',
		'directives/regular/auto-focus',
		'routes' //routes.js
	],

	function (text, $, angular) {

		$(document).ready(function () {

			angular.bootstrap(document, ['moneApp']);

		});
	}
);