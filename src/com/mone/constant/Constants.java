package com.mone.constant;

/**
 * @Class Name: Constants
 * @Description: [상수 모음 클래스]
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public final class Constants {
	
	public static final class Server {
		public static final int BAD_REQUEST_ERROR = 400;
		public static final int UNAUTHORIZED_ERROR = 403;
		public static final int SERVER_ERROR = 500;
	}
	
	// 6000번대 는 mone자체 에러 코드
	public static final class User {
		public static final int NOT_SIGNUP_ERROR = 6000;
		public static final int ALREADY_SIGNUP_ERROR = 6001;
		public static final int NOT_MATCHED_PWD_ERROR = 6002;
	}
	
	public static final class Verification {
		public static final int INVALID_TOKEN_ERROR = 10000;
		public static final int VERIFY_EMAIL_FAIL = 10001;
	}
	
	public static final class LYLSteps {
		public static final int MAINSTEP_INDEX = 0;
	}
}
