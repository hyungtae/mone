package com.mone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mone.constant.Constants;
import com.mone.exception.MException;
import com.mone.model.LYLMainStep;

public class LYLDAO {

	public int addMainStepToLYL(int signup_pk, LYLMainStep step) throws MException, Exception {
		Connection conn = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		String sql = "INSERT INTO monedb.LYL_TB(current_step, is_completed, signup_fk) VALUES (?, 0, ?)";
		int lyl_pk = -1;
		
		try {
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			query = conn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
			
			query.setInt(1, Constants.LYLSteps.MAINSTEP_INDEX);
			query.setInt(2, signup_pk);
			
			int rownum = query.executeUpdate();
			rs = query.getGeneratedKeys();
			
			if( rownum != 0 && rs.next()) {
				lyl_pk = rs.getInt(1);
				System.out.println("lyl_pk = "+lyl_pk);
			}
			
			sql = "SELECT id FROM monedb.Language_TB WHERE lng_cd=?";
			query = conn.prepareStatement(sql);
			query.setString(1, step.getWillTeachLng());
			rs = query.executeQuery();
			
			int will_teach_lng_pk = 0;
			if(rs.next()) {
				will_teach_lng_pk = rs.getInt(1);
			}
			
			sql = "SELECT id FROM monedb.Country_TB WHERE country_cd=?";
			query = conn.prepareStatement(sql);
			query.setString(1, step.getTargetCountry());
			rs = query.executeQuery();
			
			int target_country_pk = 0;
			if(rs.next()) {
				target_country_pk = rs.getInt(1);
			}
			
			sql = "SELECT id FROM monedb.TimeZone_TB WHERE tz_nm=?";
			query = conn.prepareStatement(sql);
			query.setString(1, step.getTimezone());
			rs = query.executeQuery();
			
			int timezone_pk = 0;
			if(rs.next()) {
				timezone_pk = rs.getInt(1);
			}
			
			sql = "INSERT INTO monedb.LYL_MainStep_TB (lyl_fk, will_teach_lng_fk, target_country_fk, class_limit, timezone_fk, is_navtive) VALUES (?, ?, ?, ?, ?, ?)";
			query = conn.prepareStatement(sql);
			
			
			query.setInt(1, lyl_pk);
			query.setInt(2, will_teach_lng_pk);
			query.setInt(3, target_country_pk);
			query.setInt(4, step.getLimit());
			query.setInt(5, timezone_pk);
			query.setInt(6, step.getIsNative());
			
			query.executeUpdate();
			query.close();
			
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw new MException(e);
		} finally {
			if(conn != null)
				conn.close();
		}
		
		return lyl_pk;
		
	}

}
