package com.mone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.mone.constant.Constants;
import com.mone.exception.InvalidTokenException;
import com.mone.exception.MException;
import com.mone.exception.UserCheckException;
import com.mone.model.Pair;
import com.mone.model.User;
import com.mone.util.CipherHandler;
import com.mone.util.TokenGenerator;

/**
 * @Class Name: UsersDAO
 * @Description: [사용자 관련 데이타베이스에 접근하는 클래스]
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class UsersDAO extends DBConnection {

	public ArrayList<User> queryUsers() throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		ArrayList<User> userList = new ArrayList<User>();

		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement("SELECT * FROM monedb.SignUp_TB");
			ResultSet rs = query.executeQuery();
			User user = null;
			
			while(rs.next()){
				
				user = new User();
				user.setId(rs.getInt(1));
				user.setEmail(rs.getString(2));
				user.setEmail_confirm(rs.getInt(3));
				user.setPwd(rs.getString(4));
				user.setRegist_dt(rs.getTimestamp(5));
				user.setModified_dt(rs.getTimestamp(6));
				
				userList.add(user);
			}

			query.close();
		} catch (Exception e) {
			throw new MException(e);
		} finally {
			if (conn != null)
				conn.close();
		}
		return userList;
		
		
	}

	/**
	 * @Method Description: 가입한 사용자를 DB에 추가
	 * @param email
	 * @param pwd
	 * @return 테이블의 몇 번째 행에 추가되어졌는지 int값으로 리턴(테이블의 pk)
	 * @throws MException
	 * @throws Exception
	 */
	public int addUser(String email, String pwd) throws MException, Exception {

		PreparedStatement query = null;
		Connection conn = null;
		ResultSet resultSet = null;
		int s_pk = 0;	// signup테이블에서 insert쿼리에 의해 삽입된 행의 pk
		int e_pk = 0;	// email_confirm테이블에서 insert쿼리에 의해 삽입된 행의 pk
		
		byte[] saltBytes = CipherHandler.generateSHA256Salt();	// salt값 생성
		String pwd_salt = CipherHandler.convertHexBytesToString(saltBytes);
		String encrypted_pwd = CipherHandler.encryptSHA256(pwd, saltBytes);	// salt값과 sha256으로 암호화된 비밀번호(encrytion만하고 decryption은 하지 않음)
		
		try {
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			query = conn.prepareStatement("INSERT INTO monedb.SignUp_TB(email, pwd, pwd_salt) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

			query.setString(1, email);
			query.setString(2, encrypted_pwd);
			query.setString(3, pwd_salt);
			
			int rownum = query.executeUpdate();
			resultSet = query.getGeneratedKeys();
			
			if( rownum != 0 && resultSet.next()) {
				s_pk = resultSet.getInt(1);
				System.out.println("s"+s_pk+"");
			}
			// 이메일 인증을 위한 토큰값 생성
			query = conn.prepareStatement("INSERT INTO monedb.Email_Confirm_TB(rnd_token, email_confirm, default_fk) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			query.setString(1, TokenGenerator.generate_random_token());
			query.setInt(2, 0);
			query.setInt(3, s_pk);
			
			rownum = query.executeUpdate();
			resultSet = query.getGeneratedKeys();
			
			if( rownum != 0 && resultSet.next()) {
				e_pk = resultSet.getInt(1);
			}
			
			query = conn.prepareStatement("INSERT INTO monedb.UserInfo_TB (signup_fk) VALUES (?)");
			query.setInt(1, s_pk);
			query.executeUpdate();
			
			// Signup_TB에 Email_confirm_TB를 참조하는 참조키 추가
			query = conn.prepareStatement("UPDATE monedb.SignUp_TB SET email_confirm_fk=? WHERE id= ?");
			query.setInt(1, e_pk);
			query.setInt(2, s_pk);
			query.executeUpdate();
			query.close();
			
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw new MException(e);
		} finally {
			if (conn != null)
				conn.close();
		}

		return s_pk;
	}
	
	/**
	 * @Method Description: 생성되어 있는 Email인증 관련 토큰값 반환
	 * @param email 조건절(WHERE)에서 사용되는 사용자 이메일 정보
	 * @return
	 * @throws MException
	 * @throws Exception
	 */
	public String getToken (String email) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		String result = "";
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement("SELECT ec.rnd_token FROM Email_Confirm_TB AS ec INNER JOIN SignUp_TB AS su ON ec.id = su.email_confirm_fk WHERE email=?");
			query.setString(1,email);
			ResultSet rs = query.executeQuery();
			
			if(rs.next()){
				result = rs.getString(1);
			}
			
			query.close();
		} catch (Exception e) {
			throw new MException(e);
		}
		finally {
			if(conn != null)
				conn.close();
		}

		return result;
	}
	
	/**
	 * @Method Description: 기존 랜덤 토큰값을 새로 생성된 토큰값으로 업데이트함
	 * @param email
	 * @return 새로 생성된 토큰값
	 * @throws MException
	 * @throws Exception
	 */
	public String createNewToken (String email) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		String newToken = TokenGenerator.generate_random_token();
		String result = "";
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement("UPDATE monedb.Email_Confirm_TB SET rnd_token=? WHERE id= (SELECT * FROM (SELECT ec.id FROM SignUp_TB AS su INNER JOIN Email_Confirm_TB AS ec ON su.email_confirm_fk = ec.id WHERE email=?) AS temp)");
			query.setString(1, newToken);
			query.setString(2, email);
			int rowNum = query.executeUpdate();
			
			if(rowNum > 0){
				result = newToken;
			}
			
			query.close();
		} catch (Exception e) {
			throw new MException(e);
		}
		finally {
			if(conn != null)
				conn.close();
		}

		
		return result;
	}
	
	/**
	 * @Method Description: DB에 저장된 토큰값과 이메일인증URL파라미터로 받은 토큰값을 비교하여 인증처리
	 * @param rndToken 이메일인증URL에 적힌 랜덤 토큰값
	 * @param uid 조건절(WHERE)에서 사용됨(사용자 이메일 정보)
	 * @return
	 * @throws MException
	 * @throws Exception
	 */
	public boolean ConfirmEmail(String rndToken, String uid) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		String sql = "UPDATE monedb.Email_Confirm_TB AS ec, monedb.PushSequenceNoti_TB AS psn SET ec.rnd_token='', ec.email_confirm=1, ec.completed_email_confirm_dt=NOW(), psn.is_done=1 WHERE ec.id=? AND psn.signup_fk=ec.default_fk";
		boolean result = false;
		
		try {
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			query = conn.prepareStatement("SELECT ec.id, ec.rnd_token FROM Email_Confirm_TB AS ec INNER JOIN SignUp_TB AS su ON ec.id = su.email_confirm_fk WHERE email=?");
			query.setString(1,uid);
			ResultSet rs = query.executeQuery();
			
			String dbRndToken = "";
			int id = -1;	// Email_Confirm_TB의 pk
			
			if(rs.next()){
				id = rs.getInt(1);
				dbRndToken = rs.getString(2);
			}
			
			if(!rndToken.equals(dbRndToken)){
				throw new InvalidTokenException(Constants.Verification.INVALID_TOKEN_ERROR, "유효하지 않는 토큰입니다.");
			}
			
			query = conn.prepareStatement(sql); // "UPDATE monedb.Email_Confirm_TB SET rnd_token='', email_confirm=1, completed_email_confirm_dt=NOW() WHERE id=?"
			query.setInt(1, id);
			int rowNum = query.executeUpdate();
			
			if(rowNum > 0){
				result = true;
			}
			
			query.close();
			
			conn.commit();
		} catch (Exception e) {
			conn.rollback();
			throw new MException(e);
		}
		finally {
			if(conn != null)
				conn.close();
		}

		
		return result;
	}
	
	/**
	 * @Method Description: 가입유무판별
	 * @param email
	 * @return
	 * @throws MException
	 * @throws Exception
	 */
	public boolean isExistUser(String email) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		boolean result = false;
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement("SELECT id FROM monedb.SignUp_TB WHERE email = ?");
			query.setString(1,email);
			ResultSet rs = query.executeQuery();
			
			if(rs.next()){
				result = true;
			}
			
			query.close();
		} catch (Exception e) {
			throw new MException(e);
		}
		finally {
			if(conn != null)
				conn.close();
		}

		return result;
	}
	
	/**
	 * @Method Description: 로그인처리(비밀번호가 일치한지를 판별)
	 * @param email
	 * @param pwd
	 * @return Pair 첫 번째 -이메일 인증여부를 나타내는 상수값(0-비인증, 1-인증), 두 번째 - SignUp_TB에서의 pk값
	 * @throws MException
	 * @throws Exception
	 */
	public Pair<Integer, Integer> login(String email, String pwd) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		Pair<Integer, Integer> results = new Pair<Integer, Integer>();
		//int result = -1;
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement("SELECT su.id, su.pwd, su.pwd_salt, ec.email_confirm FROM Email_Confirm_TB AS ec INNER JOIN SignUp_TB AS su ON ec.id = su.email_confirm_fk WHERE email=?");
			query.setString(1,email);
			
			ResultSet rs = query.executeQuery();
			
			if(rs.next()){
				int pk = rs.getInt(1);
				String encrypted_dbPwd = rs.getString(2);
				String pwd_dbSalt = rs.getString(3);
				
				byte[] saltBytes = CipherHandler.convertHexStringToBytes(pwd_dbSalt);	// DB로부터 가져온 salt문자열을 byte array로 변환
				String encrypted_pwd = CipherHandler.encryptSHA256(pwd, saltBytes);	// DB로부터 가져온 salt값을 기반으로 SHA256암호화

				if(!encrypted_pwd.equals(encrypted_dbPwd)){
					throw new UserCheckException(Constants.User.NOT_MATCHED_PWD_ERROR, "INVALID PASSWORD. CHECK YOUR PASSWORD.");	// 유저의 비밀번호가 일치하지 않습니다.
				}
				int email_confirm = rs.getInt(4);
				results.setFirst(email_confirm);	// 이메일 인증 여부 값
				results.setSecond(pk);				// 이메일 관련 튜플의 pk값
				//result = email_confirm;	// 이메일 인증 여부 값 리턴
				
				
				
				// result = true;
			}
			
			query.close();
		} catch (UserCheckException e) {
			throw new MException(e);
		}
		finally {
			if(conn != null)
				conn.close();
		}

		return results;
	}
}
