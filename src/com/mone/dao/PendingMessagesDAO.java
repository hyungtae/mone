package com.mone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.mone.exception.MException;

public class PendingMessagesDAO {

	public void addPendingNotiMessagesToUser (int signup_pk) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement(
					"INSERT INTO PushSequenceNoti_TB (signup_fk, noti_s_m_fk) (SELECT s.id,n.id FROM Noti_Sequence_Messages_TB AS n, SignUp_TB AS s WHERE s.id=? ORDER BY n.sequence)");
			query.setInt(1, signup_pk);
			int rowNum = query.executeUpdate();
			
			if(!(rowNum > 0)) {
				throw new MException("영향을 받은 튜플이 존재하지 않습니다.");
			}
			
			query.close();
		} catch (Exception e) {
			throw new MException(e, "초기가입자에게 알림메시지큐를 추가하는데 실패하였습니다.");
		} finally {
			if (conn != null)
				conn.close();
		}
		
	}
	
	public String getPendingSequencialNotiMessage(int signup_pk) throws MException, Exception {
		PreparedStatement query = null;
		Connection conn = null;
		/* 현재 사용자가 푸시받아야 될 순서있는 알림메시지 하나를 가져오는 쿼리 */
		String sql = "SELECT n.noti_messages FROM PushSequenceNoti_TB AS p INNER JOIN Noti_Sequence_Messages_TB AS n ON n.id=p.noti_s_m_fk WHERE p.signup_fk=? AND p.is_done=0 ORDER BY n.sequence ASC LIMIT 1";
		String result = "";
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement(sql);
			query.setInt(1, signup_pk);
			ResultSet rs = query.executeQuery();
			
			if(rs.next()) {
				result = rs.getString(1);
			}
			
			query.close();
		} catch (Exception e) {
			throw new MException(e);
		} finally {
			if(conn != null)
				conn.close();
		}
		
		return result;
	}
}
