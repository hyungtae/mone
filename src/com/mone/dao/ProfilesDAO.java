package com.mone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.mone.constant.Constants;
import com.mone.exception.MException;
import com.mone.exception.PKNotFoundException;
import com.mone.model.Profile;

/**
 * @Class Name: ProfilesDAO
 * @Description: [프로필 관련 데이타베이스에 접근하는 클래스]
 * @Created Date: 2015. 5. 6.
 * @author hyungtae
 */
public class ProfilesDAO {
	
	public Profile getUserProfileInfo (int user_pk) throws MException, Exception {
		Connection conn = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		Profile profile = new Profile();
		String sql = "SELECT first_nm, last_nm, gender, birth_dt, city, skype_id, profile_setting, country_cd, lng_cd, tz_nm FROM UserInfo_TB, " +
				"(SELECT c.country_cd, l.lng_cd, t.tz_nm FROM monedb.UserInfo_TB AS ui LEFT OUTER JOIN Country_TB AS c ON ui.nationality_fk=c.id " +
				"LEFT OUTER JOIN Language_TB AS l ON ui.first_lng_fk=l.id LEFT OUTER JOIN TimeZone_TB AS t ON ui.timezone_fk=t.id WHERE ui.id=?) AS temp WHERE id=?";
		
		try {
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			query = conn.prepareStatement(sql);
			query.setInt(1, user_pk);
			query.setInt(2, user_pk);
			rs = query.executeQuery();
			
			if(rs.next()) {
				profile.setFirstName(rs.getString(1));
				profile.setLastName(rs.getString(2));
				profile.setGender(rs.getInt(3));
				profile.setBirth_dt(rs.getDate(4));
				profile.setCity(rs.getString(5));
				profile.setSkype_username(rs.getString(6));
				profile.setProfileSetting(rs.getInt(7));
				profile.setNationality(rs.getString(8));
				profile.setFirstLng(rs.getString(9));
				profile.setTimezone(rs.getString(10));
			}
			/* 관심언어리스트 가져오기 */
			sql = "SELECT l.lng_cd FROM User_Interest_Lngs_TB AS uil INNER JOIN Language_TB AS l ON uil.lng_fk=l.id WHERE uil.user_fk=?";
			query = conn.prepareStatement(sql);
			query.setInt(1, user_pk);
			rs = query.executeQuery();
			
			ArrayList<String> iLng_arList = new ArrayList<String>();
			while(rs.next()) {
				iLng_arList.add(rs.getString(1));
			}
			
			if(iLng_arList.size() != 0) {
				String[] iLngList = new String[iLng_arList.size()];
				for(int i=0; i < iLng_arList.size(); i++) {
					iLngList[i] = iLng_arList.get(i);
				}
				profile.setYaiLngs(iLngList);
			}
			
			/* 가능한 외국어목록 가져오기 */
			sql = "SELECT l.lng_cd FROM User_Second_Lngs_TB AS usl INNER JOIN Language_TB AS l ON usl.lng_fk=l.id WHERE usl.user_fk=?";
			query = conn.prepareStatement(sql);
			query.setInt(1, user_pk);
			rs = query.executeQuery();
			
			ArrayList<String> sLng_arList = new ArrayList<String>();
			while(rs.next()) {
				sLng_arList.add(rs.getString(1));
			}
			
			if(sLng_arList.size() != 0) {
				String[] sLngList = new String[sLng_arList.size()];
				for(int i=0; i < sLng_arList.size(); i++) {
					sLngList[i] = sLng_arList.get(i);
				}
				profile.setSecondLngs(sLngList);
			}
			
			query.close();
			conn.commit();
			
		} catch (Exception e) {
			conn.rollback();
			throw new MException(e);
		} finally {
			if(conn != null)
				conn.close();
		}
		
		return profile;
	}
	
	public int getUser_pk (int signup_pk) throws MException, Exception {
		Connection conn = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		String sql = "SELECT id FROM monedb.UserInfo_TB WHERE signup_fk=?";
		int user_pk = -1;
		
		try {
			conn = DBConnection.getConnection();
			query = conn.prepareStatement(sql);
			query.setInt(1, signup_pk);
			rs = query.executeQuery();
			
			if(rs.next()){
				user_pk = rs.getInt(1);
			}
			
			if(user_pk == -1) {
				throw new PKNotFoundException(Constants.Server.SERVER_ERROR, "user_pk값을 가져올 수 없습니다.");
			}
			
			query.close();
		} catch (Exception e) {
			throw new MException(e);
		} finally {
			if(conn != null)
				conn.close();
		}
		
		return user_pk;
		
	}

	public void editUserProfile (int user_pk, Profile profile) throws Exception {
		Connection conn = null;
		PreparedStatement query = null;
		ResultSet rs = null;
		String sql = "UPDATE monedb.UserInfo_TB SET first_nm=?, last_nm=?, gender=?, birth_dt=?, city=?, skype_id=?," +
				" nationality_fk=(SELECT id FROM monedb.Country_TB WHERE country_cd=?)," +
				" first_lng_fk=(SELECT id FROM monedb.Language_TB WHERE lng_cd=?), timezone_fk=(SELECT id FROM monedb.TimeZone_TB WHERE tz_nm=?)," +
				" profile_setting=?, modified_dt=now() WHERE id=?";
		//int user_pk = -1;
		
		try {
			conn = DBConnection.getConnection();
			conn.setAutoCommit(false);
			query = conn.prepareStatement(sql);
			query.setString(1, profile.getFirstName());
			query.setString(2, profile.getLastName());
			query.setInt(3, profile.getGender());
			query.setDate(4, profile.getBirth_dt());
			query.setString(5, profile.getCity());
			query.setString(6, profile.getSkype_username());
			query.setString(7, profile.getNationality());
			query.setString(8, profile.getFirstLng());
			query.setString(9, profile.getTimezone());
			query.setInt(10, 1);
			query.setInt(11, user_pk);
			
			query.executeUpdate();
			
			/*sql = "SELECT id FROM monedb.UserInfo_TB WHERE signup_fk=?";
			query = conn.prepareStatement(sql);
			query.setInt(1, signup_pk);
			rs = query.executeQuery();
			
			if(rs.next()){
				user_pk = rs.getInt(1);
			}*/

			/* 관심언어들을 테이블에 추가 -start */
			// 선택 프로필 필드인 관심언어에 대한 pk 가져오기
			int iLngsList_length = 0;
			
			if(profile.getYaiLngs() != null){
				iLngsList_length = profile.getYaiLngs().length;
			}
			
			if(iLngsList_length == 1) {
				sql = "SELECT id AS lng_fk FROM monedb.Language_TB WHERE lng_cd=?";
			} else if (iLngsList_length > 1) {
				
				sql = "SELECT id AS lng_fk FROM monedb.Language_TB WHERE ";
				for(int i=0; i < iLngsList_length; i++) {
					sql += "lng_cd=? ";
					
					if( i != iLngsList_length - 1 ) {	// 마지막엔 추가 안함
						sql += "OR ";
					}
				}
			} else {
				sql = "DELETE FROM monedb.User_Interest_Lngs_TB WHERE user_fk=(SELECT * FROM ((SELECT IF((SELECT count(*) FROM monedb.User_Interest_Lngs_TB WHERE user_fk=?)>0, ?, -1))) AS temp)";
				query = conn.prepareStatement(sql);
				query.setInt(1, user_pk);
				query.setInt(2, user_pk);
				query.executeUpdate();
				sql = "";
			}
			
			if(!sql.equals("")) {	// 관심언어 목록 추가하기
				String[] lngList = profile.getYaiLngs();
				query = conn.prepareStatement(sql);
				
				for(int i=0; i < iLngsList_length; i++) {
					query.setString((i+1), lngList[i]);
				}
				rs = query.executeQuery();
				
				int i=0;
				int[] i_lngs_pk = new int[iLngsList_length];
				while(rs.next()) {
					i_lngs_pk[i++] = rs.getInt(1);	// 관심언어에 대한 pk저장
				}
				// 이미 관심언어들이 추가되어  있으면 삭제
				sql = "DELETE FROM monedb.User_Interest_Lngs_TB WHERE user_fk=(SELECT * FROM ((SELECT IF((SELECT count(*) FROM monedb.User_Interest_Lngs_TB WHERE user_fk=?)>0, ?, -1))) AS temp)";
				query = conn.prepareStatement(sql);
				query.setInt(1, user_pk);
				query.setInt(2, user_pk);
				query.executeUpdate();
				
				// 새로운 관심언어들을 테이블에 추가
				sql = "INSERT INTO monedb.User_Interest_Lngs_TB (user_fk, lng_fk) VALUES ";
				for(int j=0; j < iLngsList_length; j++) {
					sql += "(" + user_pk + ", ?)";
					
					if( j != iLngsList_length - 1 ) {	// 마지막엔 추가 안함
						sql += ", ";
					}
				}
				query = conn.prepareStatement(sql);
				for(int j=0; j < iLngsList_length; j++) {
					query.setInt((j+1), i_lngs_pk[j]);
				}
				query.executeUpdate();
				
			}/* 관심언어들을 테이블에 추가 -End */
			
			/* 세컨언어들을 테이블에 추가 -start */
			// 선택 프로필 필드인 세컨언어에 대한 pk 가져오기
			int sLngsList_length = 0;
			if(profile.getSecondLngs() != null){
				sLngsList_length = profile.getSecondLngs().length;
			}
			
			if(sLngsList_length == 1) {
				sql = "SELECT id AS lng_fk FROM monedb.Language_TB WHERE lng_cd=?";
			} else if (sLngsList_length > 1) {
				
				sql = "SELECT id AS lng_fk FROM monedb.Language_TB WHERE ";
				for(int i=0; i < sLngsList_length; i++) {
					sql += "lng_cd=? ";
					
					if( i != sLngsList_length - 1 ) {	// 마지막엔 추가 안함
						sql += "OR ";
					}
				}
			} else {
				// 이미 세컨언어들이 추가되어  있으면 삭제
				sql = "DELETE FROM monedb.User_Second_Lngs_TB WHERE user_fk=(SELECT * FROM ((SELECT IF((SELECT count(*) FROM monedb.User_Second_Lngs_TB WHERE user_fk=?)>0, ?, -1))) AS temp)";
				query = conn.prepareStatement(sql);
				query.setInt(1, user_pk);
				query.setInt(2, user_pk);
				query.executeUpdate();
				sql = "";
			}
			
			if(!sql.equals("")) {	// 세컨언어들을 목록에 추가하기
				String[] lngList = profile.getSecondLngs();
				query = conn.prepareStatement(sql);
				
				for(int i=0; i < sLngsList_length; i++) {
					query.setString((i+1), lngList[i]);
				}
				rs = query.executeQuery();
				
				int i=0;
				int[] s_lngs_pk = new int[sLngsList_length];
				while(rs.next()) {
					s_lngs_pk[i++] = rs.getInt(1);	// 세컨언어에 대한 pk저장
				}
				// 이미 세컨언어들이 추가되어  있으면 삭제
				sql = "DELETE FROM monedb.User_Second_Lngs_TB WHERE user_fk=(SELECT * FROM ((SELECT IF((SELECT count(*) FROM monedb.User_Second_Lngs_TB WHERE user_fk=?)>0, ?, -1))) AS temp)";
				query = conn.prepareStatement(sql);
				query.setInt(1, user_pk);
				query.setInt(2, user_pk);
				query.executeUpdate();
				
				// 새로운 관심언어들을 테이블에 추가
				sql = "INSERT INTO monedb.User_Second_Lngs_TB (user_fk, lng_fk) VALUES ";
				for(int j=0; j < sLngsList_length; j++) {
					sql += "(" + user_pk + ", ?)";
					
					if( j != sLngsList_length - 1 ) {	// 마지막엔 추가 안함
						sql += ", ";
					}
				}
				query = conn.prepareStatement(sql);
				for(int j=0; j < sLngsList_length; j++) {
					query.setInt((j+1), s_lngs_pk[j]);
				}
				query.executeUpdate();
				
			}/* 세컨언어들을 테이블에 추가 -End */
			
			query.close();
			
			conn.commit();
			
		} catch (Exception e) {
			conn.rollback();
			throw new MException(e);
		} finally {
			if(conn != null)
				conn.close();
		}
	}
	
}
