package com.mone.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * @Class Name: DBConnection
 * @Description: [DB연결 클래스] 
 * moneDB를 lookup하고 연결함
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class DBConnection {
	
	protected static Connection getConnection(){
		Connection con = null;
		
		try{
			Context init = new InitialContext();
			DataSource ds = (DataSource)init.lookup("java:comp/env/jdbc/moneDB");
			con = ds.getConnection();
		}catch(Exception e){
			e.printStackTrace();
		}finally{

		}
		
		return con;
	}

}

