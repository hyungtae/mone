package com.mone.service;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.mone.constant.Constants;
import com.mone.dao.UsersDAO;
import com.mone.exception.MException;
import com.mone.exception.UserCheckException;
import com.mone.model.Pair;
import com.mone.model.User;
import com.mone.smtp.EmailSender;


/**
 * @Class Name: UsersService
 * @Description: [UsersDAO객체에 원하는 DB처리를 요구하는 서비스클래스]
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class UsersService {
	private UsersDAO dao;
	
	public UsersService(){
		dao = new UsersDAO();
	}
	
	public String getAllUsersInfo() throws MException{
		ArrayList<User> userList;
		
		try {
			userList = dao.queryUsers();
		} catch (Exception e) {
			throw new MException(e);
		}
		
		Gson gson = new Gson();
		String result = "";
		
		if(!userList.isEmpty()){		// 성공적으로 데이타를 가져옴
			result = gson.toJson(userList);
    	}
		
		return result;
	}
	

	/**
	 * @Method Description: 이메일인증URL을 사용자에게 보냄(EmailSender객체가 실제로 보냄)
	 * @param email 사용자 이메일정보
	 * @throws MException
	 */
	public void sendEmailConfirmUrlToUser (String email) throws MException {
		EmailSender es = new EmailSender();
		String rndToken;

		try {
			rndToken = dao.getToken(email);			
			es.sendEmailConfirmUrlToUser(email, rndToken);	// 이메일 인증을 위한 url을 가입자에게 발송
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new MException(e);
		}
	}
	
	

	/**
	 * @Method Description: 토큰을 재생성하여 새로운 이메일인증URL을 사용자에게 보냄(EmailSender객체가 실제로 보냄)
	 * @param email
	 * @throws MException
	 */
	public void sendNewEmailConfirmUrlToUser (String email) throws MException {
		EmailSender es = new EmailSender();
		String rndToken;

		try {
			rndToken = dao.createNewToken(email);			
			es.sendEmailConfirmUrlToUser(email, rndToken);	// 이메일 인증을 위한 url을 가입자에게 발송
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new MException(e);
		}
	}
	
	/**
	 * @Method Description: 이메일 인증처리 
	 * @param rndToken 인증URL에 적힌 토큰값
	 * @param uid 사용자 이메일
	 * @throws MException
	 */
	public void confirmUserEmail (String rndToken, String uid) throws MException {
		try {
			if(!dao.ConfirmEmail(rndToken, uid)) {
				throw new MException(Constants.Verification.VERIFY_EMAIL_FAIL, "이메일 검증에 실패하였습니다.");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new MException(e);
		}
	}
	
	/**
	 * @Method Description: 이메일로 가입하기
	 * @param email
	 * @param pwd
	 * @return 테이블의 몇 번째 행에 추가되어졌는지 int값으로 리턴
	 * @throws MException
	 */
	public int signupToMone(String email, String pwd) throws MException {
		int pk = -1;
		try {
			if(isExistUser(email)){	// 가입된 유저인지 체크
				throw new UserCheckException(Constants.User.ALREADY_SIGNUP_ERROR, "That email address is already in use."); // 이미 등록된 이메일입니다.
			}
			
			pk = dao.addUser(email, pwd);
			
			// dao.addPendingAlertMessages(signup_pk);	// 사용자가 받아야할 알림 메시지를 추가(PushSequenceNoti_TB에 추가됨)
			
		} catch (UserCheckException e1){
			throw new MException(e1);
		} catch (Exception e2) {
			throw new MException(e2);
		}
		
		return pk;
	}

	/**
	 * @Method Description: 로그인하기
	 * @param email
	 * @param pwd
	 * @return Pair 첫 번째 -이메일 인증여부를 나타내는 상수값(0-비인증, 1-인증), 두 번째 - SignUp_TB에서의 pk값
	 * @throws MException
	 */
	public Pair<Integer, Integer> login(String email, String pwd) throws MException {
		
		try{
			if(!isExistUser(email)) {	// 가입한 유저인지 체크
				throw new UserCheckException(Constants.User.NOT_SIGNUP_ERROR, "NOT REGISTED EMAIL.");	// 등록되지 않은 이메일입니다.
			}
			
			return dao.login(email, pwd); // 비밀번호가 일치하지 않으면  에러코드가 USER_NOT_MATCHED_PWD인 UserCheckException을 던짐	
			
		} catch (UserCheckException e1) {
			throw new MException(e1);
		} catch (Exception e2) {
			throw new MException(e2);
		}

	}

	/**
	 * @Method Description: 가입유무 판별
	 * @param email
	 * @return 이메일이 DB에 이미 존재하면 true를 리턴, 그렇지 않으면 false (실제로는 exception 던짐)
	 * @throws MException
	 */
	private boolean isExistUser(String email) throws MException {
		try {
			return dao.isExistUser(email);
		} catch (Exception e) {
			throw new MException(e);
		}
	}
}
