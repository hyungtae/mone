package com.mone.service;

import com.mone.dao.PendingMessagesDAO;
import com.mone.exception.MException;

/**
 * @Class Name: PendingMessageService
 * @Description: [사용자에게 보내줄 메시지들 처리하는 서비스 클래스] 
 * @Created Date: 2015. 4. 18.
 * @author hyungtae
 */
public class PendingMessagesService {
	private PendingMessagesDAO dao;
	
	public PendingMessagesService(){
		dao = new PendingMessagesDAO();
	}
	
	/**
	 * @Method Description: 사용자가 받아야할 알림 메시지를 추가(PushSequenceNoti_TB에 추가됨)
	 * @param user_pk 메시지를 받아야할 대상의 pk(Signup_TB의 pk)
	 */
	public void addPendingNotiMessagesToUser (int user_pk) throws MException {
		try {
			dao.addPendingNotiMessagesToUser(user_pk);
		} catch (Exception e) {
			throw new MException(e);
		}
	}
	
	public String getNotiMessages (int user_pk) throws MException {	// 새로운 메시지를 가져오는 메서드 추가해야함 - getInboxNewMessages(); 메시지들을 취합해서 리턴해야함
		try {
			return dao.getPendingSequencialNotiMessage(user_pk);	// 보류중인 순차적인 알림메시지 한 개 가져오기
		} catch (Exception e) {
			throw new MException(e);
		}

	}
	
}
