package com.mone.service;

import com.mone.dao.ProfilesDAO;
import com.mone.exception.MException;
import com.mone.model.Profile;

/**
 * @Class Name: ProfileService
 * @Description: [ProfilesDAO객체에 원하는 DB처리를 요구하는 서비스클래스]
 * @Created Date: 2015. 5. 6.
 * @author hyungtae
 */
public class ProfilesService {
	private ProfilesDAO dao;
	
	public ProfilesService(){
		dao = new ProfilesDAO();
	}
	
	public void editUserProfileInfo (int signup_pk, Profile profile) throws MException {
		try {
			int user_pk = dao.getUser_pk(signup_pk);
			dao.editUserProfile(user_pk, profile);
		} catch (Exception e) {
			throw new MException(e);
		}
	}
	
	public Profile getUserProfileInfo (int signup_pk) throws MException {
		try {
			int user_pk = dao.getUser_pk(signup_pk);
			return dao.getUserProfileInfo(user_pk);
		} catch (Exception e) {
			throw new MException(e);
		}
	}
}
