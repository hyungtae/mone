package com.mone.service;

import com.mone.dao.LYLDAO;
import com.mone.exception.MException;
import com.mone.model.LYLMainStep;

public class LYLService {
	private LYLDAO dao;
	
	public LYLService(){
		dao = new LYLDAO();
	}
	
	public int addMainStepToLYL (int signup_pk, LYLMainStep step) throws MException {
		try{
			return dao.addMainStepToLYL(signup_pk, step);
		} catch (Exception e) {
			throw new MException(e);
		}
		
	}
}
