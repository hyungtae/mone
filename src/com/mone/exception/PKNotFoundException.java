package com.mone.exception;

public class PKNotFoundException extends MException {
	private static final long serialVersionUID = 1L;

	public PKNotFoundException(Throwable throwable) {
		super(throwable);
	}
	
	public PKNotFoundException(int errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}
}