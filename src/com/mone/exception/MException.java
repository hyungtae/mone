package com.mone.exception;

/**
 * @Class Name: MException
 * @Description: Mone프로젝트 최상위 예외객체 
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class MException extends Exception {
	private static final long serialVersionUID = 1L;
	protected int errorCode = 500;		// Default Server Error code(500)
	protected String errorMsg = "";
	
	public MException (Throwable throwable) {
		super(throwable);
		
		if(throwable instanceof MException) {
			setErrorCode(((MException) throwable).getErrorCode());
			setErrorMsg(((MException) throwable).getErrorMsg());
		}
	}
	
	public MException (String errorMsg) {
		super(errorMsg);
	}
	
	public MException (int errorCode, String errorMsg) {
		super(errorMsg);
		setErrorCode(errorCode);
		setErrorMsg(errorMsg);
	}

	public MException (Throwable throwable, String errorMsg) {
		super(errorMsg, throwable);
	}

	private void setErrorCode (int errorCode) {
		this.errorCode = errorCode;
	}
	
	private void setErrorMsg (String errorMsg) {
		this.errorMsg = errorMsg;
	}
	
	public int getErrorCode () {
		return errorCode;
	}
	
	public String getErrorMsg () {
		return errorMsg;
	}
	
}
