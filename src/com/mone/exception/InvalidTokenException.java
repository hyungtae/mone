package com.mone.exception;

/**
 * @Class Name: InvalidTokenException
 * @Description: 유효하지 않은 토큰관련 예외
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class InvalidTokenException extends MException {
	private static final long serialVersionUID = 1L;

	public InvalidTokenException(Throwable throwable) {
		super(throwable);
	}
	
	public InvalidTokenException(int errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}
}
