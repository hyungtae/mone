package com.mone.exception;

/**
 * @Class Name: BadRequestException
 * @Description: 클라이언트에서 전달한 데이타에 스크립트를 포함되어 있다면, 이 예외를 던짐 
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class BadRequestException extends MException {
	private static final long serialVersionUID = 1L;

	public BadRequestException(Throwable throwable) {
		super(throwable);
	}
	
	public BadRequestException(int errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}
}
