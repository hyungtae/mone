package com.mone.exception;

/**
 * @Class Name: UserCheckException
 * @Description: 사용자 정보가 유효하지 않다면, 이예외를 던짐(ex: 로그인 비밀번호) 
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class UserCheckException extends MException {
	private static final long serialVersionUID = 1L;

	public UserCheckException(Throwable throwable) {
		super(throwable);
	}
	
	public UserCheckException(int errorCode, String errorMsg) {
		super(errorCode, errorMsg);
	}
}
