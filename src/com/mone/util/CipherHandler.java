package com.mone.util;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

/**
 * @Class Name: CipherHandler
 * @Description: [암호화 관련 처리 클래스]
 * AES128, SHA256 알고리즘을 사용할 수 있음
 * AES128은 이메일인증URL을 보낼 때, uid를 암호화 및 복호화시키는데 사용
 * SHA256은 사용자 비밀번호 암호화시 사용
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class CipherHandler {

	private final static String AES_SECRETKEY = "ca4e6dad8b90bedc9934be3aba2099b2";
	private static byte[] skey_bytes = convertHexStringToBytes(AES_SECRETKEY);
	private static SecretKeySpec skeySpec = new SecretKeySpec(skey_bytes, "AES");
	private static Cipher cipher;
	
	public static byte[] convertHexStringToBytes(String hexStr) {
		if (hexStr == null || hexStr.length() == 0) {
            return null;
        }

        byte[] bytes = new byte[hexStr.length() / 2];
        
        for (int i = 0; i < bytes.length; i++) {
        	bytes[i] = (byte) Integer.parseInt(hexStr.substring(2 * i, 2 * i + 2), 16);
        }
        
        return bytes;
	}

	public static String convertHexBytesToString(byte[] hexbytes) {

		if (hexbytes == null || hexbytes.length == 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer(hexbytes.length * 2);
        String hexNumber;
        
        for (int x = 0; x < hexbytes.length; x++) {
            hexNumber = "0" + Integer.toHexString(0xff & hexbytes[x]);
            sb.append(hexNumber.substring(hexNumber.length() - 2));
        }
        
        return sb.toString();
	}

	public static byte[] generateSHA256Salt() {
		Random random = new Random();
		byte[] saltBytes = new byte[32];
		random.nextBytes(saltBytes);

		return saltBytes;
	}

	public static String encryptSHA256(String source, byte[] salt) {
		String result = "";
		try {
			byte[] a = source.getBytes();
			byte[] bytes = new byte[a.length + salt.length];
			System.arraycopy(a, 0, bytes, 0, a.length);
			System.arraycopy(salt, 0, bytes, a.length, salt.length);

			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(bytes);

			byte[] byteData = md.digest();

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; ++i) {
				sb.append(Integer.toString((byteData[i] & 0xFF) + 256, 16)
						.substring(1));
			}

			result = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return result;
	}
	
	public static String encryptAES128 (String source) {
		try {
			cipher = Cipher.getInstance("AES");
		    cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		    byte[] encrypted = cipher.doFinal(source.getBytes());
		    
		    return Hex.encodeHexString(encrypted);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public static String decryptAES128 (String source) {
		byte[] encrypted = convertHexStringToBytes(source);
		try {
			cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		    byte[] original = cipher.doFinal(encrypted);
		    
		    return new String(original);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return "";
	}
}
