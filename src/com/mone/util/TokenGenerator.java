package com.mone.util;

import java.util.Random;

/**
 * @Class Name: TokenGenerator
 * @Description: [토큰 생성 클래스]
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class TokenGenerator {
	static final String base_string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static Random rand = new Random();
	
	/**
	 * @Method Description: 랜덤한 토큰값 생성 
	 * @Created Date: 2015. 4. 16.
	 * @return
	 */
	public static String generate_random_token() {
		int length = base_string.length();
		return generate_random_string(length);
	}
	
	/**
	 * @Method Description: 사용자에게 6자리의 랜덤한 비밀번호를 리턴
	 * @Created Date: 2015. 4. 16.
	 * @return
	 */
	public static String generate_random_password() {
		int length = 6;
		return generate_random_string(length);
	}
	
	/**
	 * @Method Description: 실제적으로 랜덤한 토큰값을 만드는 메서드(generate_random_token메서드에서 호출)
	 * @Created Date: 2015. 4. 16.
	 * @param length
	 * @return
	 */
	private static String generate_random_string(int length) {

        StringBuilder token = new StringBuilder(length);
		for( int i = 0; i < length; i++ ) 
			token.append(base_string.charAt(rand.nextInt(base_string.length())));
		return token.toString();		
	}
}
