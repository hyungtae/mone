package com.mone.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.mone.constant.Constants;
import com.mone.exception.BadRequestException;
import com.mone.exception.MException;
import com.mone.model.User;

/**
 * @Class Name: DataChecker
 * @Description: [데이타체크클래스]
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class DataChecker {
	
	/**
	 * @Method Description: 사용자가 보낸 데이타가 스크립트를 포함하고 있는지를 체크
	 * @Created Date: 2015. 4. 16.
	 * @param user
	 * @return 
	 * @throws MException
	 */
	public static boolean isDataClean(User user) throws MException {
		String origin_email = user.getEmail();
		String origin_pwd = user.getPwd();
		String sanitizedEmail = user.getSanitizedData().getEmail();
		String sanitizedPwd = user.getSanitizedData().getPwd();
		
		if(!origin_email.equals(sanitizedEmail) || !origin_pwd.equals(sanitizedPwd)){	// 폼데이타에 스크립트(악성)가 포함되어있다면
			
			// BadRequest-Server was not able to process your bad request including code. Please fix your form data.
			// 데이타에 스크립트가 포함되어 있습니다.
			throw new MException(new BadRequestException(Constants.Server.BAD_REQUEST_ERROR, "WE HAVE DETECTED DANGEROUS CODE. YOU MAY BE IN DANGER OF HACKING."));	 
		}
		
		return true;
	}
	
	/**
	 * @Method Description: 
	 * @param username 스카이프 사용자 이름(ID)
	 * @return false - 스카이프에 가입되어있지 않음, true - 스카이프에 가입되어 있음 
	 * @throws MException 
	 */
	public static boolean verifySkypeUserName(String username) throws MException {
        if (username.equals("") || username == null) {
            throw new MException("Skype's username is empty");
        }
        
        username = username.toLowerCase();
        String pattern = "^[a-z][a-z0-9\\.,\\-_]{5,31}$";
        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);
        // Now create matcher object.
        Matcher m = r.matcher(username);
        if (m.find()) {
            System.out.println("Found value: " + m.group(0));
            String url = "https://login.skype.com/json/validator?new_username=" + username;
            System.out.println("Url is  - " + url);
            String s = "";
			
            try {
				s = readUrl(url);
			} catch (Exception e) {
				throw new MException(e, "I/O Exception 발생 in readUrl()");
			}
			
            if (s.contains("200")) {
                System.out.println("가입되어 있지 않은 아이디입니다.");
                return false;
            } else {
                System.out.println("이런 아이디가 이미 존재합니다.");
                return true;
            }


        } else {
            System.out.println("유효하지 않은 형식의 아이디입니다.");
            return false;
        }
    }


    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
