package com.mone.smtp;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.mone.exception.MException;
import com.mone.util.CipherHandler;

/**
 * @Class Name: EmailSender
 * @Description: [AWS SMTP SERVER에 이메일을 전달하는 클래스]
 * 이메일 인증URL을 사용자의 이메일로 전달 
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class EmailSender {
	private static final String FROM = "noreply@mone-global.com";
	private static final String DOMAIN_ADRESS = "http://192.168.0.7:8080/Mone";
    private static final String SMTP_USERNAME = "AKIAITGHEQ26MVNRGUSA";
    private static final String SMTP_PASSWORD = "AnsCw6lNEbPUIGCm5i3OTOpYf60xoP6BlerpTKcPf83Z";
    // Amazon SES SMTP host name.
    private static final String HOST = "email-smtp.us-west-2.amazonaws.com";    
    // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
    // STARTTLS to encrypt the connection.
    private static final int PORT = 25;
	
	/**
	 * @Method Description: SMTP를 사용하여 사용자에게 이메일 인증 URL 보냄
	 * @Created Date: 2015. 4. 16.
	 * @param TO
	 * @param rndToken
	 * @throws MException
	 * @throws Exception
	 */
	public void sendEmailConfirmUrlToUser (String TO, String rndToken) throws MException, Exception {
		String encrypted_uid = CipherHandler.encryptAES128(TO);
		String userName = TO.split("@")[0];
		String url = DOMAIN_ADRESS + "/api/users/email_confirm?code=" + rndToken + "&uid=" + encrypted_uid;
		String BODY = "<p style='line-height:1.8'>Hi, "+userName+"!<br><b>click this url to activate your Mone account: </b></p>"
	    		+ "<a href='" + url+ "'>" + url + "</a><br><br>Thanks.";
	    String SUBJECT = "Activate your Mone account.";
	    
		// Create a Properties object to contain connection configuration information.
    	Properties props = System.getProperties();
    	props.put("mail.transport.protocol", "smtp");
    	props.put("mail.smtp.port", PORT); 
    	
    	// Set properties indicating that we want to use STARTTLS to encrypt the connection.
    	// The SMTP session will begin on an unencrypted connection, and then the client
        // will issue a STARTTLS command to upgrade to an encrypted connection.
    	props.put("mail.smtp.auth", "true");
    	props.put("mail.smtp.starttls.enable", "true");
    	props.put("mail.smtp.starttls.required", "true");

        // Create a Session object to represent a mail session with the specified properties. 
    	Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);
        Transport transport = null;
        try
        {
	        msg.setFrom(new InternetAddress(FROM));
	        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
	        msg.setSubject(SUBJECT);
	        msg.setContent(BODY,"text/html; charset=utf-8");
	            
	        // Create a transport.        
	        transport = session.getTransport();
	                    
	        // Send the message.
        
            System.out.println("Attempting to send an email through the Amazon SES SMTP interface...");
            
            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
        	
            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            System.out.println("Email sent!");
        }
        catch (Exception e) {
        	System.out.println("The email was not sent.");
            System.out.println("Error message: " + e.getMessage());
        	throw new MException(e);
        }
        finally
        {
            // Close and terminate the connection.
            transport.close();        	
        }
	}
}
