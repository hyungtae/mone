package com.mone.model;

public class LYLMainStep {
	private String willTeachLng = "";
	private String targetCountry = "";
	private int limit;
	private String timezone = "";
	private int isNative;
	
	public void setWillTeachLng(String willTeachLng) {
		this.willTeachLng = willTeachLng;
	}
	
	public void setTargetCountry(String targetCountry) {
		this.targetCountry = targetCountry;
	}
	
	public void setLimit(int limit) {
		this.limit = limit;
	}
	
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	public void setIsNative(int isNative) {
		this.isNative = isNative;
	}
	
	public String getWillTeachLng() {
		return willTeachLng;
	}
	
	public String getTargetCountry() {
		return targetCountry;
	}
	
	public int getLimit() {
		return limit;
	}
	
	public String getTimezone() {
		return timezone;
	}
	
	public int getIsNative() {
		return isNative;
	}

}
