package com.mone.model;

import java.sql.Date;


public class Profile {
	private String firstName = "";
	private String lastName = "";
	private int gender;
	private Date birth_dt;
	private String email = "";
	private String nationality = "";
	private String city = "";
	private String firstLng = "";
	private String skype_username = "";
	private String[] yaiLngs;
	private String[] secondLngs;
	private String timezone = "";
	private int profileSetting;
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setGender(int gender) {
		this.gender = gender;
	}
	
	public void setBirth_dt(Date birth_dt) {
		this.birth_dt = birth_dt;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setFirstLng(String firstLng) {
		this.firstLng = firstLng;
	}
	
	public void setSkype_username(String skype_username) {
		this.skype_username = skype_username;
	}
	
	public void setYaiLngs(String[] yaiLngs) {
		this.yaiLngs = yaiLngs;
	}
	
	public void setSecondLngs(String[] secondLngs) {
		this.secondLngs = secondLngs;
	}
	
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	public void setProfileSetting(int profileSetting) {
		this.profileSetting = profileSetting;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public int getGender() {
		return gender;
	}
	
	public Date getBirth_dt() {
		return birth_dt;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getNationality() {
		return nationality;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getFirstLng() {
		return firstLng;
	}
	
	public String getSkype_username() {
		return skype_username;
	}
	
	public String[] getYaiLngs() {
		return yaiLngs;
	}
	
	public String[] getSecondLngs() {
		return secondLngs;
	}
	
	public String getTimezone() {
		return timezone;
	}

	public int getProfileSetting() {
		return profileSetting;
	}
	
}
