package com.mone.model;

import java.util.HashMap;

import com.google.gson.annotations.Expose;

/**
 * @Class Name: SuccessMsg
 * @Description: [성공메시지 모델 클래스]
 * 성공코드와 메시지를 클라이언트단에 전달하는데 사용
 * 저장된 데이타들은 전달되기 전, JSON데이타로 변경되어짐 
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class SuccessMsg {
	@Expose
	private int code;
	@Expose
	private String msg;
	@Expose
	private HashMap<?, ?> responseData;
	
	public SuccessMsg setCode(int code) {
		this.code = code;
		return this;
	}
	
	public SuccessMsg setMsg(String msg) {
		this.msg = msg;
		return this;
	}
	
	public SuccessMsg setData(HashMap<?, ?> data) {
		this.responseData = data;
		return this;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getMsg() {
		return msg;
	}

	public HashMap<?, ?> getData() {
		return responseData;
	}

}
