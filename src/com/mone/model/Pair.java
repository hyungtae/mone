package com.mone.model;

public class Pair <A, B> {
	private A a;
	private B b;
	
	public Pair(){}
	
	public void setFirst (A a) {
		this.a = a;
	}
	
	public void setSecond (B b) {
		this.b = b;
	}
	
	public A getFirst () {
		return a;
	}
	
	public B getSecond () {
		return b;
	}
}
