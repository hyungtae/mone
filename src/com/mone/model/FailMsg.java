package com.mone.model;

/**
 * @Class Name: FailMsg
 * @Description: [실패메시지 모델 클래스]
 * 에러코드와 에러메시지를 클라이언트단에 전달하는데 사용
 * 저장된 데이타들은 전달되기 전, JSON데이타로 변경되어짐
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class FailMsg {
	private int code;
	private String msg;
	
	public FailMsg setCode(int code) {
		this.code = code;
		return this;
	}
	
	public FailMsg setMsg(String msg) {
		this.msg = msg;
		return this;
	}
	
	public int getCode() {
		return code;
	}
	
	public String getMsg() {
		return msg;
	}

}
