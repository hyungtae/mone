package com.mone.model;

import java.sql.Timestamp;

import com.google.gson.annotations.Expose;

/**
 * @Class Name: User
 * @Description: [사용자 모델 클래스] 
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class User {
	private int id;
	@Expose
	private String email;
	@Expose
	private int email_confirm;
	private String pwd;
	private boolean remember;
	private SanitizedUser sanitizedData;
	private Timestamp regist_dt;
	private Timestamp modified_dt;
	
	public void setId(int id) {
		this.id = id;
	}
	
	public User setEmail(String email) {
		this.email = email;
		return this;
	}
	
	public User setEmail_confirm(int email_confirm) {
		this.email_confirm = email_confirm;
		return this;
	}
	
	public User setPwd(String pwd) {
		this.pwd = pwd;
		return this;
	}
	
	public User setRemember(boolean remember) {
		this.remember = remember;
		return this;
	}
	
	public User setSanitizedData(SanitizedUser sanitizedData) {
		this.sanitizedData = sanitizedData;
		return this;
	}
	
	public User setRegist_dt(Timestamp timestamp) {
		this.regist_dt = timestamp;
		return this;
	}
	
	public User setModified_dt(Timestamp modified_dt) {
		this.modified_dt = modified_dt;
		return this;
	}
	
	public int getId() {
		return id;
	}
	
	public String getEmail() {
		return email;
	}
	
	public int getEmail_confirm() {
		return email_confirm;
	}
	
	public String getPwd() {
		return pwd;
	}
	
	public boolean isRemember() {
		return remember;
	}
	
	public SanitizedUser getSanitizedData() {
		return sanitizedData;
	}
	
	public Timestamp getRegist_dt() {
		return regist_dt;
	}
	
	public Timestamp getModified_dt() {
		return modified_dt;
	}

}
