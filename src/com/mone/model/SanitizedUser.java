package com.mone.model;


/**
 * @Class Name: SanitizedUser
 * @Description: [살균화된 사용자정보 모델 클래스]
 * 안전한 데이타보장
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
public class SanitizedUser {
	private String email;
	private String pwd;
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getEmail() {
		return email;
	}
	
	public String getPwd() {
		return pwd;
	}
	
}
