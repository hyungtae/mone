package com.mone.res;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mone.constant.Constants;
import com.mone.exception.MException;
import com.mone.model.FailMsg;
import com.mone.model.Profile;
import com.mone.model.SuccessMsg;
import com.mone.model.User;
import com.mone.service.ProfilesService;

/**
 * @Class Name: Profile
 * @Description: [사용자 프로필 리소스 클래스]
 * @Created Date: 2015. 5. 6.
 * @author hyungtae
 */
@Path("/profiles")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Profiles {

	
	/**
	 * @Method Description: 프로필 정보 가져오기
	 */
	@Path("/request_user_profile")
	@GET
	public Response getUserProfileInfo (String incomingData, @Context HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Profile profile = null;
		HashMap<String, Profile> responseData = new HashMap<String, Profile>();
		//Gson gson = new Gson();
		Gson gson = new GsonBuilder()
		   .setDateFormat("yyyy-MM-dd").create();
		ProfilesService s = new ProfilesService();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		String user_email = "";
		int signup_pk = -1;
		
		if(session != null) {
			user_email = (String) session.getAttribute("uid");
			signup_pk = (Integer) session.getAttribute("upk");
		}
		
		try {
			profile = s.getUserProfileInfo(signup_pk);
			profile.setEmail(user_email);
			responseData.put("profile", profile);
			successMsg.setCode(200).setData(responseData);
			responseMsg_json = gson.toJson(successMsg);
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg(e.getMessage());
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		return Response.ok(responseMsg_json).build();
	}
	
	/**
	 * @Method Description: 프로필 정보 수정
	 */
	@Path("/edit")
	@PUT
	public Response editUserProfileInfo(String incomingData, @Context HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Profile profile = null;
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		ProfilesService s = new ProfilesService();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		int signup_pk = -1;
		System.out.println(incomingData);
		if(incomingData != null && !incomingData.equals("")) {
			profile = gson.fromJson(incomingData, Profile.class);
		}
		
		if(session != null) {
			signup_pk =  (Integer) session.getAttribute("upk");
		}
		
		try {
			s.editUserProfileInfo(signup_pk, profile);
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		
		successMsg.setCode(200).setMsg("profile change success!");
		responseMsg_json = gson.toJson(successMsg);
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(responseMsg_json).build();
	}
}
