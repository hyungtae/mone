package com.mone.res;

import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.mone.constant.Constants;
import com.mone.exception.MException;
import com.mone.model.FailMsg;
import com.mone.model.SuccessMsg;
import com.mone.util.DataChecker;

@Path("/skype")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Skype {

	@Path("/validator")
	@GET
	public Response verifySkypeUserName(@QueryParam("new_username") String skype_username) {
		Gson gson = new Gson();
		HashMap<String, Boolean> responseData = new HashMap<String, Boolean>();	// 클라이언트로 보낼 유저 데이타(successMsg의 필드로 존재)
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		try {
			if(DataChecker.verifySkypeUserName(skype_username)) {
				// 유효한(가입된) 스카이프 아이디
				responseData.put("isVerified_SUN", true);
				successMsg.setCode(200).setData(responseData);
				responseMsg_json = gson.toJson(successMsg);
			} else {
				responseData.put("isVerified_SUN", false);
				successMsg.setCode(200).setData(responseData);
				responseMsg_json = gson.toJson(successMsg);
			}
			
		} catch (MException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg(e.getMessage());
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		return Response.ok(responseMsg_json).build();
	}
}
