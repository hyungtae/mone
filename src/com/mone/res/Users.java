package com.mone.res;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mone.constant.Constants;
import com.mone.exception.MException;
import com.mone.model.FailMsg;
import com.mone.model.Pair;
import com.mone.model.SuccessMsg;
import com.mone.model.User;
import com.mone.service.PendingMessagesService;
import com.mone.service.UsersService;
import com.mone.util.CipherHandler;
import com.mone.util.DataChecker;

/**
 * @Class Name: Users
 * @Description: [사용자 리소스 클래스]
 * @Created Date: 2015. 4. 16.
 * @author hyungtae
 */
@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Users {
	
	@GET
	public Response returnUserInfoAll() {
		String usersData = null;
		UsersService s = new UsersService();
		
		try {
			usersData = s.getAllUsersInfo();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Server was not able to process your request").build();
		}
		
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(usersData).build();
	}
	
	/**
	 * @Method Description: 가입하기
	 * @param incomingData 가입정보
	 * @param request
	 * @return
	 */
	@POST
	public Response signupToMone(String incomingData, @Context HttpServletRequest request){
		User user = null;
		Gson gson = new Gson();
		UsersService us = new UsersService();
		PendingMessagesService ps = new PendingMessagesService();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responeMsg_json = "";
		int user_pk = -1;
		
		if(incomingData != null && !incomingData.equals("")) {
			user = gson.fromJson(incomingData, User.class);
		}
		
		try {
			if(DataChecker.isDataClean(user)){
				if(user != null) {
					user_pk = us.signupToMone(user.getEmail(), user.getPwd());
					ps.addPendingNotiMessagesToUser(user_pk);
				}
			}			
				
		} catch (MException e) {
			e.printStackTrace();
			
			switch (e.getErrorCode()) {
			case Constants.User.ALREADY_SIGNUP_ERROR: case Constants.Server.BAD_REQUEST_ERROR:
				failMsg.setCode(e.getErrorCode()).setMsg(e.getErrorMsg());
				responeMsg_json = gson.toJson(failMsg);
				break;
				
			default:	// 이외 예외는 서버 500에러로
				failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
				responeMsg_json = gson.toJson(failMsg);
				break;
			}
			
			return Response.status(500).entity(responeMsg_json).build();
		}
		// 세션 설정
		HttpSession session = request.getSession();
		session.setAttribute("uid", user.getEmail());
		session.setAttribute("upk", user_pk);
		session.setAttribute("eml_cfm", 0);	// 이메일 인증 기본값 0 
		System.out.println(session.getId());
		
		successMsg.setCode(200).setMsg("signup Success!");
		responeMsg_json = gson.toJson(successMsg);
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(responeMsg_json).build();
	}
	
	/**
	 * @Method Description: 사용자가 이메일 인증 URL요청한 것을 처리
	 * @param incomingData
	 * @param request
	 * @return
	 */
	@Path("/request_email_confirm")
	@POST
	public Response sendEmailConfirm (String incomingData, @Context HttpServletRequest request) {
		System.out.println(incomingData);
		Gson gson = new Gson();
		UsersService s = new UsersService();		
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responeMsg_json = "";
		
		try {
			s.sendEmailConfirmUrlToUser(incomingData);
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
			responeMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responeMsg_json).build();
		}
		
		successMsg.setCode(200).setMsg("send confirmation URL success!");
		responeMsg_json = gson.toJson(successMsg);
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(responeMsg_json).build();
	}
	
	/**
	 * @Method Description: 사용자가 새로운 이메일인증 URL요청한 것을 처리
	 * @param incomingData 사용자 이메일정보
	 * @param request
	 * @return
	 */
	@Path("/request_new_email_confirm")
	@POST
	public Response sendNewEmailConfirm (String incomingData, @Context HttpServletRequest request) {
		System.out.println(incomingData);
		Gson gson = new Gson();
		UsersService s = new UsersService();		
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responeMsg_json = "";
		
		try {
			s.sendNewEmailConfirmUrlToUser(incomingData);
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("We've failed to send a new confirmation email. Please retry.");
			responeMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responeMsg_json).build();
		}
		
		successMsg.setCode(200).setMsg("We've sent a new confirmation email.");
		responeMsg_json = gson.toJson(successMsg);
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(responeMsg_json).build();
	}
	
	/**
	 * @Method Description: 사용자가 접근권한이 있는지를 체크 
	 * @param incomingData
	 * @param request
	 * @return
	 */
	@Path("/auth")
	@POST
	public Response authenticate(String incomingData, @Context HttpServletRequest request){
		//세션이 없으면 재생성하지 않는다 (=getsession(false))
		HttpSession session = request.getSession(false);
		User user = new User();
		HashMap<String, User> responseData = new HashMap<String, User>();	// 클라이언트로 보낼 유저 데이타(successMsg의 필드로 존재)
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";

		if(session != null) {
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();	// 노출되지 않는 데이타는 json파싱에서 제외(@Expose annotation 이용)
			String user_email = (String) session.getAttribute("uid");
			Integer ec_result =  (Integer) session.getAttribute("eml_cfm");
			responseData.put("user", user.setEmail(user_email));
			responseData.put("email_confirm", user.setEmail_confirm(ec_result));
			successMsg.setCode(200).setData(responseData);
			responseMsg_json = gson.toJson(successMsg);
			
			return Response.ok(responseMsg_json).build();
		} else {
			Gson gson = new Gson();
			failMsg.setCode(Constants.Server.UNAUTHORIZED_ERROR).setMsg("Access denied and unauthorized.");
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
	}
	
	/**
	 * @Method Description: 이메일인증 처리(인증URL을 가입자가 클릭)
	 * @param code 랜덤 토큰값
	 * @param uid AES128로 암호화된 사용자 이메일 정보 
	 * @param request
	 * @return
	 */
	@Path("/email_confirm")
	@GET
	public Response confirmEmail (@QueryParam("code") String code, @QueryParam("uid") String uid, @Context HttpServletRequest request) {
		Gson gson = new Gson();
		UsersService s = new UsersService();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		
		String email = CipherHandler.decryptAES128(uid);	// uid = 사용자 이메일 주소
		try {
			s.confirmUserEmail(code, email);
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg(e.getMessage());
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		HttpSession session = request.getSession();
		session.setAttribute("eml_cfm", 1);	// email_confirm - value
		
		successMsg.setCode(200).setMsg("Succeeded in confirming email!");
		responseMsg_json = gson.toJson(successMsg);
		return Response.ok(responseMsg_json).build();
	}
	
	
	/**
	 * @Method Description: 로그인하기
	 * @param incomingData
	 * @param request
	 * @return
	 */
	@Path("/login")
	@POST
	public Response login(String incomingData, @Context HttpServletRequest request){
		System.out.println("call!!"+incomingData);
		User user = null;
		Gson gson = new Gson();
		UsersService s = new UsersService();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		//int ecResult = -1;		// 이메일 인증 상수값
		Pair<Integer, Integer> results = null;
		
		if(incomingData != null && !incomingData.equals("")) {
			user = gson.fromJson(incomingData, User.class);
		}
		
		try {
			if(DataChecker.isDataClean(user)){
				// ecResult = s.login(user.getEmail(), user.getPwd());
				results = s.login(user.getEmail(), user.getPwd());
			}
		} catch (MException e) {
			e.printStackTrace();
			
			switch (e.getErrorCode()) {
			case Constants.Server.BAD_REQUEST_ERROR: case Constants.User.NOT_SIGNUP_ERROR: case Constants.User.NOT_MATCHED_PWD_ERROR:
				failMsg.setCode(e.getErrorCode()).setMsg(e.getErrorMsg());
				responseMsg_json = gson.toJson(failMsg);
				break;
				
			default:	// 이외 예외는 서버 500에러로
				failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
				responseMsg_json = gson.toJson(failMsg);
				break;
			}
			
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		
		// 세션 설정
		HttpSession session = request.getSession();
		session.setAttribute("uid", user.getEmail());
		session.setAttribute("upk", results.getSecond());		// pk value
		session.setAttribute("eml_cfm", results.getFirst());	// email_confirm value
		// System.out.println(session.getId());
		// System.out.println("upk - " + results.getSecond());
		// System.out.println("email_confirm value - " + results.getFirst());
		
		successMsg.setCode(200).setMsg("LOGIN SUCCESS!");
		responseMsg_json = gson.toJson(successMsg);
				
		if(user.isRemember()){	
			// 3일 동안 세션과 쿠기 유지
			Date expdate= new Date();
			expdate.setTime (expdate.getTime() + (3*24*60*60*1000));
			DateFormat df = new SimpleDateFormat("dd MMM yyyy kk:mm:ss z");
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
			session.setMaxInactiveInterval( 3*24*60*60);
			
			return Response.ok(responseMsg_json).header("Set-Cookie", "m_suid="+session.getId()+"; Path=/Mone/; Max-Age="+3*24*60*60+"; expires="+df.format(expdate)+"; HttpOnly").build();
		}
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(responseMsg_json).build();

	}
	
	/**
	 * @Method Description: 로그아웃
	 * @param incomingData
	 * @param request
	 * @return
	 */
	@Path("/logout")
	@POST
	public Response logout(String incomingData, @Context HttpServletRequest request){
		Gson gson = new Gson();		
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";

		HttpSession session = request.getSession(false);
		
		if(session == null) {
			failMsg.setCode(500).setMsg("Session doesn't exist.");
			responseMsg_json = gson.toJson(failMsg);
			
			return Response.status(500).entity(responseMsg_json).build();
		} else {
			session.invalidate();
			successMsg.setCode(200).setMsg("Logged out.");
			responseMsg_json = gson.toJson(successMsg);
			// cookie path 설정이 정확해야 쿠키가 삭제됨 
			// ie는 max-age를 지원 안함 그래서 expires를 씀
			return Response.ok(responseMsg_json).header("Set-Cookie", "m_suid=; Path=/Mone/; Max-Age=0; expires=0; HttpOnly").build();
		}
	}
	
	/**
	 * @Method Description: 
	 * @param incomingData
	 * @param request
	 * @return
	 */
	@Path("/noti")
	@POST
	public Response getNotiMessages(String incomingData, @Context HttpServletRequest request){
		Gson gson = new Gson();
		HashMap<String, ArrayList<String>> responseData = new HashMap<String, ArrayList<String>>();	// 클라이언트로 보낼 유저 데이타(successMsg의 필드로 존재)
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		int user_pk = -1;
		
		HttpSession session = request.getSession(false);
		
		if(session != null) {
			user_pk =  (Integer) session.getAttribute("upk");
		}
		
		PendingMessagesService ps = new PendingMessagesService();
		
		try{
			String noti_msg = ps.getNotiMessages(user_pk);
			
			ArrayList<String> list = new ArrayList<String>();
			list.add(noti_msg);
			
			responseData.put("noti_msgs", list);
			successMsg.setCode(200).setData(responseData);
			responseMsg_json = gson.toJson(successMsg);
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg(e.getMessage());
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		return Response.ok(responseMsg_json).build();
	}
}
