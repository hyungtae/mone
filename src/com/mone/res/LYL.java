package com.mone.res;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.mone.constant.Constants;
import com.mone.exception.MException;
import com.mone.model.FailMsg;
import com.mone.model.LYLMainStep;
import com.mone.model.SuccessMsg;
import com.mone.service.LYLService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Path("/lyl")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LYL {
	@Path("/auth")
	@GET
	public Response checkLYLAuth(String incomingData, @Context HttpServletRequest request,@QueryParam("newLngId") int newLngId) {
		HttpSession session = request.getSession(false);
		Gson gson = new Gson();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		
		try {
			if(session != null && session.getAttribute("lyl_pk") != null) {
				int lyl_pk = (Integer) session.getAttribute("lyl_pk");
				if(lyl_pk > 0 && lyl_pk == newLngId) {
					successMsg.setCode(200).setMsg("success!");
					responseMsg_json = gson.toJson(successMsg);
					
				} else {
					throw new MException(Constants.Server.SERVER_ERROR, "lyl_pk value is not matched.");
				}
			} else {
				throw new MException(Constants.Server.SERVER_ERROR, "session is null or lyl_pk value is not Found");
			}
		
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		return Response.ok(responseMsg_json).build();
		
	}

	@Path("/main_step")
	@POST
	public Response addMainStepToLYL (String incomingData, @Context HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		LYLMainStep step = null;
		Gson gson = new Gson();
		LYLService s = new LYLService();
		HashMap<String, Integer> responseData = new HashMap<String, Integer>();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		int signup_pk = -1;
		
		String responseMsg_json = "";
		if(incomingData != null && !incomingData.equals("")) {
			step = gson.fromJson(incomingData, LYLMainStep.class);
		}
		
		if(session != null) {
			signup_pk =  (Integer) session.getAttribute("upk");
		}
		
		try {
			int lyl_pk = s.addMainStepToLYL(signup_pk, step);
			
			if(lyl_pk != -1) {
				session.setAttribute("lyl_pk", lyl_pk);	// 나중에 단계가 추가될 때마다 사용되기 때문에 세션에 값을 저장
				responseData.put("newLngId", lyl_pk);
			}
			
		} catch (MException e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
		
		
		successMsg.setCode(200).setData(responseData).setMsg("Main Step add success in ListYourLanguage proccess!");
		responseMsg_json = gson.toJson(successMsg);
		// Response.ok() means we will be creating a http 200 status code.
		return Response.ok(responseMsg_json).build();
	}
	
	@Path("/photo_step")
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@Context ServletContext context, @FormDataParam("files") InputStream fileInputStream, @FormDataParam("files") FormDataContentDisposition contentDispositionHeader) {
		Gson gson = new Gson();
		SuccessMsg successMsg = new SuccessMsg();
		FailMsg failMsg = new FailMsg();
		String responseMsg_json = "";
		
		System.out.println("Upload Request is submitted to this server...");
		
		
        //String filePath = context.getRealPath("uploads") +"\\"+ contentDispositionHeader.getFileName();
		String filePath = "C:\\Users\\hyungtae\\web_workspace\\Mone\\WebContent\\uploads\\"+ contentDispositionHeader.getFileName();
        
		try {
			// save the file to the server
	        saveFile(fileInputStream, filePath);
		} catch (Exception e) {
			e.printStackTrace();
			
			failMsg.setCode(Constants.Server.SERVER_ERROR).setMsg("Server was not able to process your request");
			responseMsg_json = gson.toJson(failMsg);
			return Response.status(500).entity(responseMsg_json).build();
		}
        

        String output = "File saved to server location : " + filePath;
        System.out.println(output);
        successMsg.setCode(200).setMsg("File is uploaded to the server...");
		responseMsg_json = gson.toJson(successMsg);
		
        return Response.ok(responseMsg_json).build();
    }

    // save uploaded file to a defined location on the server
    private void saveFile(InputStream uploadedInputStream, String serverLocation) {
        try {
            OutputStream outpuStream = new FileOutputStream(new File(serverLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            outpuStream = new FileOutputStream(new File(serverLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }

            outpuStream.flush();
            outpuStream.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

}
